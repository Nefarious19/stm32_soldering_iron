/*
 * tim1.c
 *
 *  Created on: 25.02.2019
 *      Author: rafal
 */
#include "stm32f0xx.h"
#include "tim1.h"

void TIM1_EncoderModeInit(ENCODER_TYPE_t mode)
{
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
	GPIOA->MODER |= (2<<GPIO_MODER_MODER8_Pos) | (2<<GPIO_MODER_MODER9_Pos);
	GPIOA->AFR[1] |= (2<<GPIO_AFRH_AFRH0_Pos) | (2<<GPIO_AFRH_AFRH1_Pos);

	TIM1->CR1 	 |= TIM_CR1_ARPE;

	TIM1->CCMR1 |= TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_0; /* (1)*/
	TIM1->CCER &= (uint16_t)(~ (TIM_CCER_CC1P | TIM_CCER_CC2P)); /* (2) */

	if(mode == ENCODER_MODE_NORMAL) TIM1->CCER	 |= TIM_CCER_CC1P;
	else TIM1->CCER	 &= ~TIM_CCER_CC1P;

	TIM1->ARR	  = 0xFFFF;
	TIM1->SMCR	 |= TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1;
	TIM1->CR1	 |= TIM_CR1_CEN;
}
