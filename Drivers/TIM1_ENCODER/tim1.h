/*
 * tim1.h
 *
 *  Created on: 25.02.2019
 *      Author: rafal
 */

#ifndef TIM1_ENCODER_TIM1_H_
#define TIM1_ENCODER_TIM1_H_

typedef enum
{
	ENCODER_MODE_NORMAL,
	ENCODER_MODE_INVERSED
}ENCODER_TYPE_t;

void TIM1_EncoderModeInit(ENCODER_TYPE_t mode);

#endif /* TIM1_ENCODER_TIM1_H_ */
