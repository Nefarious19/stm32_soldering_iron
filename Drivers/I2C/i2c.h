/*
 * i2c.h
 *
 *  Created on: 10.01.2019
 *      Author: rafal
 */

#ifndef I2C_I2C_H_
#define I2C_I2C_H_

#include <stdint.h>

void I2C_init(void);
void I2C_writeByte(uint8_t slave, uint8_t data);
void I2C_writeNBytes(uint8_t slave, uint8_t * data, uint8_t len);
uint8_t I2C_readByte(uint8_t slave);
uint8_t * I2C_readNBytes(uint8_t slave, uint8_t * buffer, uint8_t len);
#endif /* I2C_I2C_H_ */
