/*
 * tim16.c
 *
 *  Created on: 16.03.2019
 *      Author: rafal
 */
#include <stm32f0xx.h>

static void ( * tim16_timer10msCallback )(void) = ( void *)(0);

void TIM16_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_TIM16EN;
	NVIC_ClearPendingIRQ(TIM16_IRQn);
	NVIC_EnableIRQ(TIM16_IRQn);

	TIM16->PSC = 48000 - 1;
	TIM16->ARR = 10 - 1;
	TIM16->DIER |= TIM_DIER_UIE;
	TIM16->CR1 = TIM_CR1_CEN;
}

void TIM16_RegisterTimer10msCallback(void(*func_ptr)(void))
{
	if(func_ptr)
	{
		tim16_timer10msCallback = func_ptr;
	}
}

void TIM16_IRQHandler (void)
{
	if(TIM16->SR & TIM_SR_UIF)
	{
		TIM16->SR &= ~TIM_SR_UIF;
		if(tim16_timer10msCallback)
		{
			tim16_timer10msCallback();
		}
	}
}
