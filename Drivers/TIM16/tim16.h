/*
 * tim16.h
 *
 *  Created on: 16.03.2019
 *      Author: rafal
 */

#ifndef TIM16_TIM16_H_
#define TIM16_TIM16_H_

void TIM16_init(void);
void TIM16_RegisterTimer10msCallback(void(*func_ptr)(void));

#endif /* TIM16_TIM16_H_ */
