/*
 * spi.c
 *
 *  Created on: 12.01.2019
 *      Author: rafal
 */


#include "stm32f0xx.h"
#include "spi.h"

static inline void SPI_CS_Low(void)
{
	GPIOA->BRR  = (1<<4);
}

static inline void SPI_CS_High(void)
{
	GPIOA->BSRR = (1<<4);
}

void SPI_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;

	GPIOA->AFR[0] &= (GPIO_AFRL_AFRL5_Msk) | (GPIO_AFRL_AFRL6_Msk);
	GPIOA->MODER  |= (2<<GPIO_MODER_MODER5_Pos) | (2<<GPIO_MODER_MODER6_Pos);
	GPIOA->MODER  |= (1<<GPIO_MODER_MODER4_Pos);
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR4 | GPIO_OSPEEDER_OSPEEDR5 | GPIO_OSPEEDER_OSPEEDR6;
	GPIOA->ODR |= (1<<4);

	SPI1->CR1 |= SPI_CR1_BR_2 |
				 SPI_CR1_SSM  |
				 SPI_CR1_SSI   |
				 SPI_CR1_MSTR |
				 SPI_CR1_RXONLY;
	SPI1->CR2 |= (7<<SPI_CR2_DS_Pos) | SPI_CR2_FRXTH;
}

void SPI_readNBytes(uint8_t * data, uint8_t len)
{
	SPI_CS_Low();
	uint8_t i = len-1;
	SPI1->CR1 |= SPI_CR1_SPE;

	while(1)
	{
		if(SPI1->SR & SPI_SR_RXNE)
		{
			*data++ = *(volatile uint8_t *)(&SPI1->DR);
			i--;
			if(!i)break;
		}
	}

	SPI1->CR1 &= ~SPI_CR1_SPE;
	while(!(SPI1->SR & SPI_SR_RXNE));
	*data = *(volatile uint8_t *)(&SPI1->DR);
	while(SPI1->SR & SPI_SR_BSY);
	SPI_CS_High();
}
