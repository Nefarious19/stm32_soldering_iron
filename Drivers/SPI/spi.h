/*
 * spi.h
 *
 *  Created on: 12.01.2019
 *      Author: rafal
 */

#ifndef SPI_SPI_H_
#define SPI_SPI_H_

void SPI_init(void);
void SPI_readNBytes(uint8_t * data, uint8_t len);

#endif /* SPI_SPI_H_ */
