/*
 * tim3.h
 *
 *  Created on: 26.02.2019
 *      Author: rafal
 */

#ifndef TIM3_PWM_TIM3_H_
#define TIM3_PWM_TIM3_H_

void TIM3_PWMInit(void);
void TIM3_PWMSetDutyCycle(uint16_t dutyCycle);

#endif /* TIM3_PWM_TIM3_H_ */
