/*
 * tim3.c
 *
 *  Created on: 26.02.2019
 *      Author: rafal
 */
#include "stm32f0xx.h"

void TIM3_PWMInit(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	GPIOA->MODER  |= (2<<GPIO_MODER_MODER7_Pos);
	GPIOA->AFR[0] |= (1<<GPIO_AFRL_AFRL7_Pos);

	TIM3->PSC   = 47; /* (1) */
	TIM3->ARR   = 99; /* (2) */
	TIM3->CCR2  = 0; /* (3) */
	TIM3->CCMR1 |= TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2PE; /* (4) */
	TIM3->CCER  |= TIM_CCER_CC2E; /* (5) */
	TIM3->BDTR  |= TIM_BDTR_MOE; /* (6) */
	TIM3->CR1   |= TIM_CR1_CEN; /* (7) */
	TIM3->EGR   |= TIM_EGR_UG; /* (8) */
}

void TIM3_PWMSetDutyCycle(uint16_t dutyCycle)
{
	dutyCycle = (dutyCycle > 99) ? 99 : dutyCycle;
	TIM3->CCR2  = dutyCycle;
}
