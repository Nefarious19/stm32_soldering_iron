/*
 * ext_int.h
 *
 *  Created on: 05.03.2019
 *      Author: rafal
 */

#ifndef EXT_INTERRUPT_EXT_INT_H_
#define EXT_INTERRUPT_EXT_INT_H_

#include <stdint.h>

typedef void ( * EXT_INT_Handler )(void);
typedef void ( * EXT_INT_ButtonHoldHandler )(void);

typedef enum
{
	EXT_INT_INTERRUPT_DIRECT,
	EXT_INT_INTERRUPT_DEBOUNCE,
}
EXT_INT_InterruptMode_t;

void EXT_INT_init(void);
void EXT_INT_ServiceTask(void);

void EXT_INT_EXT0_RegsiterHandler(EXT_INT_Handler ptr, EXT_INT_InterruptMode_t	mode );
void EXT_INT_EXT1_RegsiterHandler(EXT_INT_Handler ptr, EXT_INT_InterruptMode_t	mode );
void EXT_INT_EXT2_RegsiterHandler(EXT_INT_Handler ptr, EXT_INT_InterruptMode_t	mode );
void EXT_INT_EXT3_RegsiterHandler(EXT_INT_Handler ptr, EXT_INT_InterruptMode_t	mode );
void EXT_INT_EXT11_RegsiterHandler(EXT_INT_Handler ptr, EXT_INT_InterruptMode_t	mode );
void EXT_INT_EXT10_RegsiterHandler(EXT_INT_Handler ptr, EXT_INT_InterruptMode_t	mode );

void EXT_INT_EXT0_RegsiterButtonHoldHandler(EXT_INT_ButtonHoldHandler ptr, uint32_t holdTimeInMs );
void EXT_INT_EXT1_RegsiterButtonHoldHandler(EXT_INT_ButtonHoldHandler ptr, uint32_t holdTimeInMs );


#endif /* EXT_INTERRUPT_EXT_INT_H_ */
