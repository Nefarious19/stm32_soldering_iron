/*
 * ext_int.c
 *
 *  Created on: 05.03.2019
 *      Author: rafal
 */
#include "stm32f0xx.h"
#include "ext_int.h"

#define EXT_INT_INTERRUPTS_NUMBER 6

typedef void ( * EXT_INT_Handler )(void);

typedef enum
{
	EXT_INT_BUTTON_PRESSED,
	EXT_INT_BUTTON_DEBOUNCE,
	EXT_INT_BUTTON_RELEASED
}
EXT_INT_InterruptStateMachine_t;

typedef enum
{
	EXT_INT_INTERRUPT_CLEAR,
	EXT_INT_INTERRUPT_PENDING,
}
EXT_INT_InterruptState_t;

typedef enum
{
	EXT_INT_INTERRUPT_UNLOCKED,
	EXT_INT_INTERRUPT_LOCKED
}EXT_INT_InterruptLock_t;

typedef struct
{
	EXT_INT_Handler interrupt_handler;
	EXT_INT_ButtonHoldHandler button_hold_handler;
	EXT_INT_InterruptState_t interrupt_state;
	EXT_INT_InterruptMode_t interrupt_mode;
	EXT_INT_InterruptLock_t interrupt_lock;
	EXT_INT_InterruptStateMachine_t interrupt_state_machine;
	uint32_t interrupt_delay_timer;
	uint32_t button_hold_delay_timer_set;
	uint32_t button_hold_delay_timer_actual;
	uint8_t  button_hold_handler_done_flag;
	GPIO_TypeDef * interrupt_port_ptr;
	uint32_t interrupt_gpio_pin;
}EXT_INT_Interrupt_t;


static volatile EXT_INT_Interrupt_t EXT0 = { 0 };
static volatile EXT_INT_Interrupt_t EXT1 = { 0 };
static volatile EXT_INT_Interrupt_t EXT2 = { 0 };
static volatile EXT_INT_Interrupt_t EXT3 = { 0 };
static volatile EXT_INT_Interrupt_t EXT10 = { 0 };
static volatile EXT_INT_Interrupt_t EXT11 = { 0 };


static volatile EXT_INT_Interrupt_t * EXT_INT_Interrupt_tab[EXT_INT_INTERRUPTS_NUMBER] =
{
	&EXT0,
	&EXT1,
	&EXT2,
	&EXT3,
	&EXT10,
	&EXT11
};

void EXT_INT_init(void)
{
	GPIOA->MODER &= ~GPIO_MODER_MODER0;
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR0_0;

	GPIOA->MODER &= ~GPIO_MODER_MODER1;
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR1_0;

	GPIOA->MODER &= ~GPIO_MODER_MODER2;
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR2_0;

	GPIOA->MODER &= ~GPIO_MODER_MODER3;
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR3_0;

	//Set enc button pin as input with pullup
	GPIOA->MODER &= ~GPIO_MODER_MODER10;
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR10_0;

	//Set grid synchro pin as input with pullup
	GPIOA->MODER &= ~GPIO_MODER_MODER11;
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR11_0;

	SYSCFG->EXTICR[0] |= SYSCFG_EXTICR1_EXTI0_PA;
	SYSCFG->EXTICR[0] |= SYSCFG_EXTICR1_EXTI1_PA;
	SYSCFG->EXTICR[0] |= SYSCFG_EXTICR1_EXTI2_PA;
	SYSCFG->EXTICR[0] |= SYSCFG_EXTICR1_EXTI3_PA;

	SYSCFG->EXTICR[2] |= SYSCFG_EXTICR3_EXTI11_PA;
	SYSCFG->EXTICR[2] |= SYSCFG_EXTICR3_EXTI10_PA;

	EXTI->IMR  |= EXTI_IMR_IM0;
	EXTI->FTSR|= EXTI_FTSR_FT0;

	EXTI->IMR |= EXTI_IMR_IM1;
	EXTI->FTSR |= EXTI_FTSR_FT1;

	EXTI->IMR |= EXTI_IMR_IM2;
	EXTI->FTSR |= EXTI_FTSR_FT2;

	EXTI->IMR |= EXTI_IMR_IM3;
	EXTI->FTSR |= EXTI_FTSR_FT3;

	EXTI->IMR  |= EXTI_IMR_IM11;
	EXTI->RTSR |= EXTI_RTSR_RT11;

	EXTI->IMR |= EXTI_IMR_IM10;
	EXTI->RTSR |= EXTI_RTSR_RT10;

	EXT0.interrupt_port_ptr = GPIOA;
	EXT1.interrupt_port_ptr = GPIOA;
	EXT2.interrupt_port_ptr = GPIOA;
	EXT3.interrupt_port_ptr = GPIOA;
	EXT10.interrupt_port_ptr = GPIOA;
	EXT11.interrupt_port_ptr = GPIOA;

	EXT0.interrupt_gpio_pin = 0;
	EXT1.interrupt_gpio_pin = 1;
	EXT2.interrupt_gpio_pin = 2;
	EXT3.interrupt_gpio_pin = 3;
	EXT10.interrupt_gpio_pin = 10;
	EXT11.interrupt_gpio_pin = 11;

	EXT0.interrupt_lock = EXT_INT_INTERRUPT_UNLOCKED;
	EXT1.interrupt_lock = EXT_INT_INTERRUPT_UNLOCKED;
	EXT2.interrupt_lock = EXT_INT_INTERRUPT_UNLOCKED;
	EXT3.interrupt_lock = EXT_INT_INTERRUPT_UNLOCKED;
	EXT10.interrupt_lock = EXT_INT_INTERRUPT_UNLOCKED;
	EXT11.interrupt_lock = EXT_INT_INTERRUPT_UNLOCKED;

	NVIC_ClearPendingIRQ(EXTI4_15_IRQn);
	NVIC_ClearPendingIRQ(EXTI0_1_IRQn);
	NVIC_ClearPendingIRQ(EXTI2_3_IRQn);

	NVIC_SetPriority(EXTI4_15_IRQn,0);
	NVIC_SetPriority(EXTI0_1_IRQn,0);
	NVIC_SetPriority(EXTI2_3_IRQn,0);

	NVIC_EnableIRQ(EXTI4_15_IRQn);
	NVIC_EnableIRQ(EXTI0_1_IRQn);
	NVIC_EnableIRQ(EXTI2_3_IRQn);
}

void EXT_INT_ServiceTask(void)
{
	for(uint8_t i = 0; i <  EXT_INT_INTERRUPTS_NUMBER; i++)
	{
		volatile EXT_INT_Interrupt_t * EXT = EXT_INT_Interrupt_tab[i];

		if(EXT->interrupt_mode == EXT_INT_INTERRUPT_DEBOUNCE)
		{
			if(EXT->interrupt_state == EXT_INT_INTERRUPT_PENDING)
			{
				switch(EXT->interrupt_state_machine)
				{
					case EXT_INT_BUTTON_PRESSED:
					{
						EXT->interrupt_delay_timer = 2;
						EXT->interrupt_state_machine = EXT_INT_BUTTON_DEBOUNCE;
						if(EXT->button_hold_handler)
						{
							EXT->button_hold_delay_timer_actual = EXT->button_hold_delay_timer_set;
							EXT->button_hold_handler_done_flag = 0;
						}
					}
					break;

					case EXT_INT_BUTTON_DEBOUNCE:
					{
						if(EXT->interrupt_delay_timer && (EXT->interrupt_port_ptr->IDR & (1<<EXT->interrupt_gpio_pin)))
						{
							EXT->interrupt_delay_timer--;
						}
						else if(EXT->button_hold_handler && (!(EXT->interrupt_port_ptr->IDR & (1<<EXT->interrupt_gpio_pin))) && (EXT->button_hold_handler_done_flag==0))
						{
							if(EXT->button_hold_delay_timer_actual)
							{
								EXT->button_hold_delay_timer_actual--;
							}
							else
							{
								EXT->button_hold_handler();
								EXT->button_hold_handler_done_flag = 1;
							}
						}
						else if((EXT->interrupt_delay_timer == 0) && (EXT->interrupt_port_ptr->IDR & (1<<EXT->interrupt_gpio_pin)))
						{
							if(EXT->button_hold_handler_done_flag == 0)
							{
								EXT->interrupt_handler();
							}
							EXT->interrupt_state_machine = EXT_INT_BUTTON_RELEASED;
						}
					}
					break;

					case EXT_INT_BUTTON_RELEASED:
					{
						EXT->interrupt_state = EXT_INT_INTERRUPT_CLEAR;
						EXT->interrupt_state_machine = EXT_INT_BUTTON_PRESSED;
						EXT->interrupt_lock = EXT_INT_INTERRUPT_UNLOCKED;
					}
					break;
				}
			}
		}
	}
}

void EXT_INT_EXT0_RegsiterButtonHoldHandler(EXT_INT_ButtonHoldHandler ptr, uint32_t holdTimeInMs )
{
	EXT0.button_hold_handler = ptr;
	EXT0.button_hold_delay_timer_set = holdTimeInMs / 10;
}

void EXT_INT_EXT1_RegsiterButtonHoldHandler(EXT_INT_ButtonHoldHandler ptr, uint32_t holdTimeInMs )
{
	EXT1.button_hold_handler = ptr;
	EXT1.button_hold_delay_timer_set = holdTimeInMs / 10;
}

void EXT_INT_EXT0_RegsiterHandler(EXT_INT_Handler ptr, EXT_INT_InterruptMode_t	mode )
{
	if(ptr)
	{
		EXT0.interrupt_handler = ptr;
		EXT0.interrupt_mode = mode;
	}
}

void EXT_INT_EXT1_RegsiterHandler(EXT_INT_Handler ptr, EXT_INT_InterruptMode_t	mode )
{
	if(ptr)
	{
		EXT1.interrupt_handler = ptr;
		EXT1.interrupt_mode = mode;
	}
}

void EXT_INT_EXT2_RegsiterHandler(EXT_INT_Handler ptr, EXT_INT_InterruptMode_t	mode )
{
	if(ptr)
	{
		EXT2.interrupt_handler = ptr;
		EXT2.interrupt_mode = mode;
	}
}

void EXT_INT_EXT3_RegsiterHandler(EXT_INT_Handler ptr, EXT_INT_InterruptMode_t	mode )
{
	if(ptr)
	{
		EXT3.interrupt_handler = ptr;
		EXT3.interrupt_mode = mode;
	}
}

void EXT_INT_EXT11_RegsiterHandler(EXT_INT_Handler ptr, EXT_INT_InterruptMode_t	mode )
{
	if(ptr)
	{
		EXT11.interrupt_handler = ptr;
		EXT11.interrupt_mode = mode;
	}
}

void EXT_INT_EXT10_RegsiterHandler(EXT_INT_Handler ptr, EXT_INT_InterruptMode_t	mode )
{
	if(ptr)
	{
		EXT10.interrupt_handler = ptr;
		EXT10.interrupt_mode = mode;
	}
}



void EXTI4_15_IRQHandler (void)
{
	NVIC_ClearPendingIRQ(EXTI4_15_IRQn);

	if(EXTI->PR & EXTI_PR_PIF11)
	{
		EXTI->PR |= EXTI_PR_PIF11;

		if(EXT11.interrupt_handler && EXT11.interrupt_lock == EXT_INT_INTERRUPT_UNLOCKED)
		{


			if(EXT11.interrupt_mode == EXT_INT_INTERRUPT_DEBOUNCE)
			{
				EXT11.interrupt_lock = EXT_INT_INTERRUPT_LOCKED;
				EXT11.interrupt_state = EXT_INT_INTERRUPT_PENDING;
			}
			else
			{
				EXT11.interrupt_handler();
			}
		}
	}

	if(EXTI->PR & EXTI_PR_PIF10)
	{
		EXTI->PR |= EXTI_PR_PIF10;

		if(EXT10.interrupt_handler && EXT10.interrupt_lock == EXT_INT_INTERRUPT_UNLOCKED)
		{


			if(EXT10.interrupt_mode == EXT_INT_INTERRUPT_DEBOUNCE)
			{
				EXT10.interrupt_lock = EXT_INT_INTERRUPT_LOCKED;
				EXT10.interrupt_state = EXT_INT_INTERRUPT_PENDING;
			}
			else
			{
				EXT10.interrupt_handler();
			}
		}
	}
}


void EXTI0_1_IRQHandler (void)
{
	NVIC_ClearPendingIRQ(EXTI0_1_IRQn);

	if(EXTI->PR & EXTI_PR_PIF0)
	{
		EXTI->PR |= EXTI_PR_PIF0;

		if(EXT0.interrupt_handler && EXT0.interrupt_lock == EXT_INT_INTERRUPT_UNLOCKED)
		{


			if(EXT0.interrupt_mode == EXT_INT_INTERRUPT_DEBOUNCE)
			{
				EXT0.interrupt_state = EXT_INT_INTERRUPT_PENDING;
				EXT0.interrupt_lock = EXT_INT_INTERRUPT_LOCKED;
			}
			else
			{
				EXT0.interrupt_handler();
			}
		}
	}

	if(EXTI->PR & EXTI_PR_PIF1)
	{
		EXTI->PR |= EXTI_PR_PIF1;

		if(EXT1.interrupt_handler && EXT1.interrupt_lock == EXT_INT_INTERRUPT_UNLOCKED)
		{


			if(EXT1.interrupt_mode == EXT_INT_INTERRUPT_DEBOUNCE)
			{
				EXT1.interrupt_state = EXT_INT_INTERRUPT_PENDING;
				EXT1.interrupt_lock = EXT_INT_INTERRUPT_LOCKED;
			}
			else
			{
				EXT1.interrupt_handler();
			}
		}
	}
}

void EXTI2_3_IRQHandler (void)
{
	NVIC_ClearPendingIRQ(EXTI2_3_IRQn);

	if(EXTI->PR & EXTI_PR_PIF2)
	{
		EXTI->PR |= EXTI_PR_PIF2;

		if(EXT2.interrupt_handler && EXT2.interrupt_lock == EXT_INT_INTERRUPT_UNLOCKED)
		{
			if(EXT2.interrupt_mode == EXT_INT_INTERRUPT_DEBOUNCE)
			{
				EXT2.interrupt_lock = EXT_INT_INTERRUPT_LOCKED;
				EXT2.interrupt_state = EXT_INT_INTERRUPT_PENDING;
			}
			else
			{
				EXT2.interrupt_handler();
			}
		}
	}

	if(EXTI->PR & EXTI_PR_PIF3)
	{
		EXTI->PR |= EXTI_PR_PIF3;

		if(EXT3.interrupt_handler && EXT3.interrupt_lock == EXT_INT_INTERRUPT_UNLOCKED )
		{
			if(EXT3.interrupt_mode == EXT_INT_INTERRUPT_DEBOUNCE)
			{
				EXT3.interrupt_lock = EXT_INT_INTERRUPT_LOCKED;
				EXT3.interrupt_state = EXT_INT_INTERRUPT_PENDING;
			}
			else
			{
				EXT3.interrupt_handler();
			}
		}
	}
}
