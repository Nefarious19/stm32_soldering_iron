/*
 * lcd_display_task.c
 *
 *  Created on: 28.03.2019
 *      Author: rafal
 */
#include "stm32f0xx.h"
#include "lcd_display_task.h"

void (*currentDisplayFuncPtr)(void) = NULL;

void lcd_display_task_init(void)
{

}

void lcd_display_task(void)
{
	if(currentDisplayFuncPtr != NULL)
	{
		currentDisplayFuncPtr();
	}
}


void lcd_display_registerCurrentDisplayFunc(void(*func_ptr)(void))
{
	if(func_ptr != 0)
	{
		currentDisplayFuncPtr = func_ptr;
	}
}


