/*
 * lcd_display_task.h
 *
 *  Created on: 28.03.2019
 *      Author: rafal
 */

#ifndef LCD_DISPLAY_TASK_LCD_DISPLAY_TASK_H_
#define LCD_DISPLAY_TASK_LCD_DISPLAY_TASK_H_


void lcd_display_task_init(void);
void lcd_display_task(void);
void lcd_display_registerCurrentDisplayFunc(void(*func_ptr)(void));


#endif /* LCD_DISPLAY_TASK_LCD_DISPLAY_TASK_H_ */
