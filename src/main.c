#include "stm32f0xx.h"
#include <stdlib.h>
#include <stdio.h>
#include <i2c.h>
#include <spi.h>
#include <tim3.h>
#include <ext_int.h>
#include <tim16.h>

#include <util.h>
#include <common.h>
#include <PROG_TIMERS/prog_timers.h>
#include <FEE/fee.h>
#include <global_default_tuning_values.h>
#include "NVM_STORAGE/nvm_storage.h"

#include "../PERIPHS/BUZZER/buzzer.h"
#include "../PERIPHS/LED/led.h"
#include "../PERIPHS/MAX31855/max31855.h"
#include "../PERIPHS/ST7032/st7032.h"
#include "../PERIPHS/TRIAC_CONTROL/triac.h"
#include "../PERIPHS/ENCODER/encoder.h"

#include "PID_TASK/task_pid.h"
#include "LCD_DISPLAY_TASK/lcd_display_task.h"
#include "CALCULATE_TEMP_TASK/calculate_temp_task.h"
#include "LED_TASK/led_task.h"
#include "GUI/gui.h"

void procPeripheralDriversInit(void)
{

	initSystemClock();
	initGpioClocks();


	NVM_Init();



	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock/1000);

	I2C_init();
	SPI_init();
	EXT_INT_init();
}


void boardPeripheralDriversInit(void)
{

	NVM_EncoderParams_t encParam;
	NVM_TemperatureParams_t tempParam;
	NVM_BuzzerParams_t buzzerParam;

	NVM_GetEncoderParams(&encParam);
	NVM_GetTemperatureParams(&tempParam);
	NVM_GetBuzzerParams(&buzzerParam);

	BUZZER_Init(buzzerParam.buzzerOnOffSwitch);

	TIM1_EncoderModeInit(encParam.encoderType);
	ENCODER_Init(encParam.encoderStepVale);
	ENCODER_SetValueRange(tempParam.minimumTipTemperature, tempParam.minimumTipTemperature, tempParam.maximumTipTemperature);
	ENCODER_DisablePostionMonitoring();


	TRIAC_init();
	TRIAC_setPower(0);
}

int main(void)
{
  __disable_irq();

  NVM_LedParams_t ledParam;

  procPeripheralDriversInit();
  boardPeripheralDriversInit();

  NVM_GetLedParams(&ledParam);

  TRIAC_RegisterFullCycleEndCallback(task_pid_main);

  PROG_TIMER_Create(lcd_display_task, 100);
  PROG_TIMER_Create(calculate_temp_task, 100);
  PROG_TIMER_Create(GUI_TemperatureMonitoringTask,100);
  PROG_TIMER_Create(BUZZER_Task,10);
  PROG_TIMER_Create(EXT_INT_ServiceTask, 10);

  if(ledParam.ledOnOffSwitch == 1)
  {
	  PROG_TIMER_Create(led_blink_task, ledParam.ledBlinkTime);
	  led_blink_task_init();
  }

  lcd_display_task_init();
  calculate_temp_task_init();

  GUI_Init();

  __enable_irq();

  while (1)
  {
	  GUI_Service();
	  ENCODER_Service();
	  PROG_TIMER_Service();
	  TRIAC_Service();
  }

}

void error_handler(void)
{
	STLCD_BUF_locate(0,0);
	STLCD_BUF_puts("ERROR OCCURED   ");
	STLCD_BUF_locate(0,1);
	STLCD_BUF_puts("RESTART DEVICE  ");
	STLCD_BUF_refresh();
}



