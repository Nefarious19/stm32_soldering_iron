/*
 * interrupts.c
 *
 *  Created on: 21.07.2019
 *      Author: rafal
 */

extern void error_handler(void);


void HardFault_Handler(void)
{
	asm("NOP");
	asm("BLX error_handler" );
}


void WWDG_IRQHandler(void)
{
	asm("NOP");
	asm("BLX error_handler" );
}
