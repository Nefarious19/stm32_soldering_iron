/*
 * calculate_temp.c
 *
 *  Created on: 28.03.2019
 *      Author: rafal
 */

#include "stm32f0xx.h"

#include "calculate_temp_task.h"

#include <spi.h>
#include "../../PERIPHS/MAX31855/max31855.h"
#include "../../PERIPHS/ST7032/st7032.h"
#include "../../PERIPHS/ENCODER/encoder.h"


#include "../PID/PID.h"
#include "../PID_TASK/task_pid.h"
#include "../LCD_DISPLAY_TASK/lcd_display_task.h"
#include "../GUI/gui.h"

#include <global_default_tuning_values.h>
#include "../NVM_STORAGE/nvm_storage.h"

static int32_t desiredTipTemperature;
static NVM_TemperatureParams_t temperatureParams;

#define MEAN_SIZE 32

void calculate_temp_task_init(void)
{
	PID_Init( 6.5*SCALING_FACTOR , 0.004*SCALING_FACTOR, 12*SCALING_FACTOR, 100, 0, SCALING_FACTOR);
	MAX31855_init(SPI_readNBytes);
	NVM_GetTemperatureParams(&temperatureParams);

	switch(temperatureParams.startUpMode)
	{
		case NVM_TEMPERATURE_STARTUP_DEFAULT:
			desiredTipTemperature = temperatureParams.minimumTipTemperature;
			break;
		case NVM_TEMPERATURE_STARTUP_M1:
			desiredTipTemperature = temperatureParams.savedTemperature_M1;
			break;
		case NVM_TEMPERATURE_STARTUP_M2:
			desiredTipTemperature = temperatureParams.savedTemperature_M2;
			break;
	}

	GUI_updateDesiredTipTemp(desiredTipTemperature);
}

void calculate_temp_task(void)
{
	static uint8_t first_start = 0;
	static uint8_t counter = 0;
	static int32_t sum = 0;
	static int32_t x[MEAN_SIZE];

	int32_t y;
	MAX31855_TCTemp_t data;
	MAX31855_ChipStatus_t chip_status;


	NVM_GetTemperatureParams(&temperatureParams);

	chip_status = MAX31855_getDataFromChip().chipStatus;
	MAX31855_getTCTemperature(&data);

	if( data.tempDec > temperatureParams.maximumTipTemperature ||  data.tempDec < 0 )
	{
		chip_status = MAX31855_STATUS_WRONG_DATA_FAULT;
	}

	if(chip_status == MAX31855_STATUS_NO_FAULT)
	{

		GUI_updateCurrentChipStatus(chip_status, 0);

		if(first_start == 0)
		{
			first_start = 1;
			for(counter = 0; counter < MEAN_SIZE; counter++)
			{
				x[counter] = data.tempDec;
			}
			sum = MEAN_SIZE * data.tempDec;
			y = data.tempDec;
			GUI_updateCurrentTipTemp(y);
			counter = 0;
			task_pid_Enable();
			return;
		}

		task_pid_updateCurrentTipTemperature(data.tempDec);
		task_pid_updateDesiredTipTemperature(desiredTipTemperature);

		sum -= x[counter];
		x[counter] = data.tempDec;
		sum += x[counter];
		counter++;
		if(counter == MEAN_SIZE) counter = 0;

		y = sum/MEAN_SIZE;

		GUI_updateActualCurrentTipTemp(data.tempDec);
		GUI_updateCurrentTipTemp(y);
	}
	else
	{
		task_pid_Disable();
		GUI_updateCurrentChipStatus(chip_status, 1);
		first_start = 0;
	}
}

void calculate_temp_task_UpdateDesiredTipTemp ( int32_t desiredTipTemp )
{
	desiredTipTemperature = desiredTipTemp;
}
