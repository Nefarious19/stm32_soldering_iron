/*
 * calculate_tem_task.h
 *
 *  Created on: 28.03.2019
 *      Author: rafal
 */

#ifndef CALCULATE_TEMP_TASK_CALCULATE_TEMP_TASK_H_
#define CALCULATE_TEMP_TASK_CALCULATE_TEMP_TASK_H_



void calculate_temp_task_init(void);
void calculate_temp_task(void);
void calculate_temp_task_UpdateDesiredTipTemp ( int32_t desiredTipTemp );

#endif /* CALCULATE_TEMP_TASK_CALCULATE_TEMP_TASK_H_ */
