/*
 * led_task.h
 *
 *  Created on: 28.03.2019
 *      Author: rafal
 */

#ifndef LED_TASK_LED_TASK_H_
#define LED_TASK_LED_TASK_H_

void led_blink_task_init(void);
void led_blink_task(void);

#endif /* LED_TASK_LED_TASK_H_ */
