/*
 * led_task.c
 *
 *  Created on: 28.03.2019
 *      Author: rafal
 */

#include "stm32f0xx.h"
#include "../../PERIPHS/LED/led.h"

void led_blink_task_init(void)
{
	LED_Init();
}

void led_blink_task(void)
{
	LED_Tog();
}
