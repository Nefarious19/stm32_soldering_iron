/*
 * gui.c
 *
 *  Created on: 28.03.2019
 *      Author: rafal
 */
#include "stm32f0xx.h"
#include <string.h>
#include <FEE/fee.h>
#include <ext_int.h>
#include "gui.h"

#include "../LCD_DISPLAY_TASK/lcd_display_task.h"
#include "../CALCULATE_TEMP_TASK/calculate_temp_task.h"
#include "../PID/PID.h"

#include "../MENU/menu.h"

#include "../../PERIPHS/ST7032/st7032.h"
#include "../../PERIPHS/ENCODER/encoder.h"
#include "../../PERIPHS/BUZZER/buzzer.h"

#include "progress_bar_pattern.h"
#include "../NVM_STORAGE/nvm_storage.h"

typedef enum
{
	MODE_STABILIZING_AFTER_UPDATE,
	MODE_STABILIZING,
	MODE_STABLE,
	MODE_DISRUPTION
}GUI_TEMP_MONITOR_MODE_t;

typedef enum
{
	GUI_TEMPERATURE_STARTUP,
	GUI_TEMPERATURE_M1,
	GUI_TEMPERATURE_M2
} GUI_m1OrM2_t;

typedef struct
{
	const char * error_header;
	const char * error_description;
}gui_error_t;

typedef struct
{
	uint8_t clearVisionEnable;
	uint8_t isNewHigherThanPrev;
	uint8_t displayUpdateTime;
	int32_t valueThatShouldNotBeCrossed;
}gui_temperatureClearVision_t;

static gui_temperatureClearVision_t thClearVision;

static int32_t currentTipTemperatureActual;
static int32_t currentTipTemperature;
static int32_t prevDesiredTipTemperature;
static int32_t desiredTipTemperature;
static int32_t tempDesiredTipTemperature;
static int32_t TipTemperature = 0;

static GUI_MODE_t gui_mode = GUI_MODE_STARTUP;
static uint8_t gui_mode_updated = 0;
static uint8_t startup_screen_display_time = STARTUP_SCREEN_TIME;
static uint8_t gui_encoder_button_cntr = 0;
static uint8_t gui_TempSetModeInvokeCntr = 0;

static GUI_BUTTON_t gui_button = GUI_BUTTON_NO_BUTTON;
static uint8_t guiFaultFlag = 0;
static MAX31855_ChipStatus_t guiChipStatus = MAX31855_STATUS_NO_FAULT;
static uint8_t guiThereWasFaultFlag = 0;

static uint8_t BrightnessCounter = 0;
static uint8_t DesiredBrightness = 0;

static GUI_TEMP_MONITOR_MODE_t guiTempMonMode = MODE_STABILIZING;
static ENCODER_t enc = { 0 };
static NVM_LCDParams_t LCDParams = { 0 };
static NVM_TemperatureParams_t temperatureParams = { 0 };



static GUI_m1OrM2_t m1_or_m2 = GUI_TEMPERATURE_STARTUP;

const gui_error_t error_tab[] =
{
	{
		.error_header = "ERROR 0",
		.error_description = "NO_FAULT",
	},
	{
		.error_header = "ERROR 1",
		.error_description = "OC_FAULT",
	},
	{
		.error_header = "ERROR 2",
		.error_description = "SCG_FAULT",
	},
	{
		.error_header = "ERROR 3",
		.error_description = "SCV_FAULT",
	},
	{
		.error_header = "ERROR 4",
		.error_description = "WRONG_DATA_FAULT",
	}
};

static void GUI_setNewTipTemperature(int32_t temperature)
{
	if(guiTempMonMode == MODE_STABLE)
	{
		if(temperature > desiredTipTemperature)
		{
			thClearVision.isNewHigherThanPrev = 1;
		}
		else if(temperature < desiredTipTemperature)
		{
			thClearVision.isNewHigherThanPrev = 0;
		}
		thClearVision.clearVisionEnable = 1;
		thClearVision.valueThatShouldNotBeCrossed = desiredTipTemperature;
		prevDesiredTipTemperature = desiredTipTemperature;
		thClearVision.displayUpdateTime = 4;
	}
	else
	{
		thClearVision.clearVisionEnable = 0;
		thClearVision.isNewHigherThanPrev = 0;
		thClearVision.valueThatShouldNotBeCrossed = 0;
		prevDesiredTipTemperature = currentTipTemperature;
		thClearVision.displayUpdateTime = 2;
	}

	desiredTipTemperature = temperature;
	calculate_temp_task_UpdateDesiredTipTemp(desiredTipTemperature);
	guiTempMonMode = MODE_STABILIZING_AFTER_UPDATE;
	BUZZER_SetBuzzerPeriod(100);
}

static void GUI_StartUpModeView(void)
{
	const char greetUp[]   = " SOLDER STATION ";
	const char greetDown[] = "powered by STM32 ";
	static uint8_t charCntrUp = 0;
	static uint8_t charCntrDown = 0;


	if(charCntrUp < strlen(greetUp))
	{
		STLCD_BUF_locate(charCntrUp,0);
		STLCD_BUF_putc(greetUp[charCntrUp]);
    	charCntrUp++;
	}
	else if(charCntrDown < strlen(greetDown))
	{
		STLCD_BUF_locate(charCntrDown,1);
		STLCD_BUF_putc(greetDown[charCntrDown]);
    	charCntrDown++;
	}


	STLCD_BUF_refresh();

	startup_screen_display_time--;

	if(startup_screen_display_time == 2)
	{
		BUZZER_On();
	}
	else if(startup_screen_display_time == 0)
	{
		BUZZER_Off();
		GUI_changeCurrentViewMode(GUI_MODE_NORMAL);
		TipTemperature = currentTipTemperature;
	}
}

static void GUI_TempSetModeView(void)
{

	tempDesiredTipTemperature = ENCODER_GetValue();

	STLCD_BUF_locate(0,0);
	STLCD_BUF_puts("T set:    ");
	STLCD_BUF_locate(0,1);
 	STLCD_BUF_puts("T actual: ");
	STLCD_BUF_locate(10,0);
	STLCD_BUF_puts("      ");
	STLCD_BUF_locate(10,0);
	if(tempDesiredTipTemperature < 100)
	{
		STLCD_BUF_putc('0');
	}
	STLCD_BUF_putsInt(tempDesiredTipTemperature, 10);
	STLCD_BUF_putc(0b11011111);
	STLCD_BUF_putc('C');
	STLCD_BUF_locate(10,1);
	STLCD_BUF_puts("      ");
	STLCD_BUF_locate(10,1);
	if(TipTemperature < 100)
	{
		STLCD_BUF_putc(' ');
	}
	STLCD_BUF_putsInt(TipTemperature, 10);
	STLCD_BUF_putc(0b11011111);
	STLCD_BUF_putc('C');

	gui_TempSetModeInvokeCntr++;

	if(gui_TempSetModeInvokeCntr < 3)
	{
		switch(gui_encoder_button_cntr)
		{
			case 1:
				STLCD_BUF_locate(10,0);
				STLCD_BUF_putc(' ');
			break;

			case 2:
				STLCD_BUF_locate(11,0);
				STLCD_BUF_putc(' ');
			break;

			case 3:
				STLCD_BUF_locate(12,0);
				STLCD_BUF_putc(' ');
			break;
		}
	}

	if(gui_TempSetModeInvokeCntr == 6)
	{
		gui_TempSetModeInvokeCntr = 0;
	}

	STLCD_BUF_refresh();
}

static void GUI_NormalModeView(void)
{
	if(DesiredBrightness > BrightnessCounter)
	{
		BrightnessCounter++;
		STLCD_setBacklightBrigtness(BrightnessCounter);
	}

	STLCD_BUF_locate(0,0);
	STLCD_BUF_puts("T set: ");

	if(m1_or_m2 == GUI_TEMPERATURE_M1)
	{
		STLCD_BUF_puts("M1  ");
	}
	else if(m1_or_m2 == GUI_TEMPERATURE_M2)
	{
		STLCD_BUF_puts("M2  ");
	}
	else
	{
		STLCD_BUF_puts("    ");
	}

	STLCD_BUF_locate(0,1);
 	STLCD_BUF_puts("T actual: ");
 	STLCD_BUF_locate(10,0);
	STLCD_BUF_puts("      ");
	STLCD_BUF_locate(10,0);
	if(desiredTipTemperature < 100)
	{
		STLCD_BUF_putc(' ');
	}
	STLCD_BUF_putsInt(desiredTipTemperature, 10);
	STLCD_BUF_putc(0b11011111);
	STLCD_BUF_putc('C');
	STLCD_BUF_locate(10,1);
	STLCD_BUF_puts("      ");
	STLCD_BUF_locate(10,1);
	if(TipTemperature < 100)
	{
		STLCD_BUF_putc(' ');
	}
	STLCD_BUF_putsInt(TipTemperature, 10);
	STLCD_BUF_putc(0b11011111);
	STLCD_BUF_putc('C');
	STLCD_BUF_refresh();
}

static void GUI_SleepModeView(void)
{
	if(DesiredBrightness < BrightnessCounter)
	{
		BrightnessCounter--;
		STLCD_setBacklightBrigtness(BrightnessCounter);
	}

	STLCD_BUF_locate(0,0);
	STLCD_BUF_puts("T set:  ");
	STLCD_BUF_putc(6);
	STLCD_BUF_putc(' ');
	STLCD_BUF_locate(0,1);
 	STLCD_BUF_puts("T actual: ");
	STLCD_BUF_locate(10,0);
	STLCD_BUF_puts("      ");
	STLCD_BUF_locate(10,0);
	if(desiredTipTemperature < 100)
	{
		STLCD_BUF_putc(' ');
	}
	STLCD_BUF_putsInt(desiredTipTemperature, 10);
	STLCD_BUF_putc(0b11011111);
	STLCD_BUF_putc('C');
	STLCD_BUF_locate(10,1);
	STLCD_BUF_puts("      ");
	STLCD_BUF_locate(10,1);
	if(TipTemperature < 100)
	{
		STLCD_BUF_putc(' ');
	}
	STLCD_BUF_putsInt(TipTemperature, 10);
	STLCD_BUF_putc(0b11011111);
	STLCD_BUF_putc('C');
	STLCD_BUF_refresh();

}



static void GUI_MenuModeView(void)
{
	MENU_Show();
}

static void GUI_ErrorModeView(void)
{
	STLCD_BUF_clr();
	STLCD_BUF_puts(error_tab[guiChipStatus].error_header);
	STLCD_BUF_locate(0,1);
	STLCD_BUF_puts(error_tab[guiChipStatus].error_description);
	STLCD_BUF_refresh();
}

static void GUI_encoderButtonPressedCallback(void)
{
	if( gui_mode == GUI_MODE_NORMAL || gui_mode == GUI_MODE_TEMP_SET )
	{
		gui_encoder_button_cntr++;
		switch(gui_encoder_button_cntr)
		{
			case 1:
				GUI_changeCurrentViewMode(GUI_MODE_TEMP_SET);
				ENCODER_SetMulValue(100);
				ENCODER_SetValue(desiredTipTemperature);
				ENCODER_EnablePostionMonitoring();
			break;

			case 2:
				ENCODER_SetMulValue(10);
			break;

			case 3:
				ENCODER_SetMulValue(1);
			break;

			case 4:
				NVM_GetTemperatureParams(&temperatureParams);
				ENCODER_DisablePostionMonitoring();
				GUI_changeCurrentViewMode(GUI_MODE_NORMAL);
				GUI_setNewTipTemperature(tempDesiredTipTemperature);
				gui_encoder_button_cntr = 0;
				gui_TempSetModeInvokeCntr = 0;

				if(tempDesiredTipTemperature == temperatureParams.savedTemperature_M1)
				{
					m1_or_m2 = GUI_TEMPERATURE_M1;
				}
				else if(tempDesiredTipTemperature == temperatureParams.savedTemperature_M2)
				{
					m1_or_m2 = GUI_TEMPERATURE_M2;
				}
				else
				{
					m1_or_m2 = GUI_TEMPERATURE_STARTUP;
				}

			break;
		}
	}
	else if( gui_mode == GUI_MODE_MENU )
	{
		gui_button = GUI_BUTTON_ACTION;
	}

}

static void GUI_UP_ButtonPressedCallback(void)
{
	gui_button = GUI_BUTTON_UP;
}

static void GUI_DOWN_ButtonPressedCallback(void)
{
	gui_button = GUI_BUTTON_DOWN;
}

static void GUI_MENU_ButtonPressedCallback(void)
{
	if( gui_mode == GUI_MODE_NORMAL )
	{
		ENCODER_GetAllParams(&enc);
		ENCODER_SetValueRange(-1, 0, 1);
		ENCODER_EnablePostionMonitoring();
		GUI_changeCurrentViewMode(GUI_MODE_MENU);
		gui_button = GUI_BUTTON_ACTION;
	}
	else
	{
		gui_button = GUI_BUTTON_MENU;
	}
}

static void GUI_SLEEP_ButtonPressedCallback(void)
{

	gui_button = GUI_BUTTON_SLEEP;

}

void GUI_ButtonUpHoldCallback(void)
{
	NVM_GetTemperatureParams(&temperatureParams);
	m1_or_m2 = GUI_TEMPERATURE_M1;
	GUI_setNewTipTemperature(desiredTipTemperature);
	temperatureParams.savedTemperature_M1 = desiredTipTemperature;
	NVM_SetTemperatureParams(&temperatureParams);
}

void GUI_ButtonDownHoldCallback(void)
{
	NVM_GetTemperatureParams(&temperatureParams);
	m1_or_m2 = GUI_TEMPERATURE_M2;
	GUI_setNewTipTemperature(desiredTipTemperature);
	temperatureParams.savedTemperature_M2 = desiredTipTemperature;
	NVM_SetTemperatureParams(&temperatureParams);
}

void GUI_Init(void)
{

	NVM_GetLCDParams(&LCDParams);
	NVM_GetTemperatureParams(&temperatureParams);

	switch(temperatureParams.startUpMode)
	{
		case NVM_TEMPERATURE_STARTUP_DEFAULT:
			desiredTipTemperature = temperatureParams.minimumTipTemperature;
			m1_or_m2 = GUI_TEMPERATURE_STARTUP;
			break;
		case NVM_TEMPERATURE_STARTUP_M1:
			desiredTipTemperature = temperatureParams.savedTemperature_M1;
			m1_or_m2 = GUI_TEMPERATURE_M1;
			break;
		case NVM_TEMPERATURE_STARTUP_M2:
			desiredTipTemperature = temperatureParams.savedTemperature_M2;
			m1_or_m2 = GUI_TEMPERATURE_M2;
			break;
	}

	STLCD_init();
	STLCD_setBacklightBrigtness(LCDParams.backlightBrightness);
	STLCD_setContrast(LCDParams.contrast);
	DesiredBrightness = LCDParams.backlightBrightness;
	BrightnessCounter = LCDParams.backlightBrightness;

	STLCD_setCGRAMPattern(0,bar0);
	STLCD_setCGRAMPattern(1,bar1);
	STLCD_setCGRAMPattern(2,bar2);
	STLCD_setCGRAMPattern(3,bar3);
	STLCD_setCGRAMPattern(4,bar4);
	STLCD_setCGRAMPattern(5,bar5);
	STLCD_setCGRAMPattern(6,moon);

	ENCODER_RegisterButtonPressedCallback(GUI_encoderButtonPressedCallback);
	EXT_INT_EXT0_RegsiterHandler(GUI_UP_ButtonPressedCallback, EXT_INT_INTERRUPT_DEBOUNCE);
	EXT_INT_EXT1_RegsiterHandler(GUI_DOWN_ButtonPressedCallback, EXT_INT_INTERRUPT_DEBOUNCE);
	EXT_INT_EXT2_RegsiterHandler(GUI_MENU_ButtonPressedCallback, EXT_INT_INTERRUPT_DEBOUNCE);
	EXT_INT_EXT3_RegsiterHandler(GUI_SLEEP_ButtonPressedCallback, EXT_INT_INTERRUPT_DEBOUNCE);
	EXT_INT_EXT0_RegsiterButtonHoldHandler(GUI_ButtonUpHoldCallback, 2500);
	EXT_INT_EXT1_RegsiterButtonHoldHandler(GUI_ButtonDownHoldCallback, 2500);

	MENU_init(&MAIN_MENU);

	prevDesiredTipTemperature = temperatureParams.minimumTipTemperature;
	guiTempMonMode = MODE_STABILIZING;

}

static inline void GUI_compareAndUpdateTemperature(void)
{
	if(TipTemperature < currentTipTemperature )
	{
		TipTemperature++;
	}
	else if(TipTemperature > currentTipTemperature)
	{
		TipTemperature--;
	}
}

void GUI_TemperatureMonitoringTask(void)
{

	static uint8_t errorOccurenceCounter = 0;
	volatile static uint8_t internalCounter = 0;
	static uint8_t waitForGo = 0;

	if(!guiFaultFlag)
	{
		if(guiThereWasFaultFlag)
		{
			guiThereWasFaultFlag = 0;
			errorOccurenceCounter = 0;
			GUI_changeCurrentViewMode(GUI_MODE_NORMAL);
		}

		if(errorOccurenceCounter) errorOccurenceCounter--;

		internalCounter++;
		if(internalCounter > thClearVision.displayUpdateTime) internalCounter = 0;

		switch(guiTempMonMode)
		{
			case MODE_STABILIZING_AFTER_UPDATE:
			{
				if(thClearVision.clearVisionEnable)
				{
					if(thClearVision.isNewHigherThanPrev)
					{
						if(currentTipTemperature < thClearVision.valueThatShouldNotBeCrossed)
						{
							TipTemperature = thClearVision.valueThatShouldNotBeCrossed;
						}
						else
						{
							if(internalCounter == 0)
							{
								GUI_compareAndUpdateTemperature();
							}
						}
					}
					else
					{
						if(currentTipTemperature > thClearVision.valueThatShouldNotBeCrossed)
						{
							TipTemperature = thClearVision.valueThatShouldNotBeCrossed;
						}
						else
						{
							if(internalCounter == 0)
							{
								GUI_compareAndUpdateTemperature();
							}
						}
					}
				}
				else
				{
					if(internalCounter == 0)
					{
						GUI_compareAndUpdateTemperature();
					}
				}

				if(currentTipTemperature == desiredTipTemperature || waitForGo)
				{
					if(waitForGo == 0)
					{
						waitForGo = 1;
						thClearVision.clearVisionEnable = 0;
						thClearVision.isNewHigherThanPrev = 0;
						thClearVision.valueThatShouldNotBeCrossed = 0;
					}
					else
					{
						if(TipTemperature == desiredTipTemperature)
						{
							waitForGo = 0;
							guiTempMonMode = MODE_STABLE;
							BUZZER_SetBuzzerPeriod(300);
							thClearVision.displayUpdateTime = 2;
						}
					}
				}
			}
			break;

			case MODE_STABILIZING:
			{
				if(internalCounter == 0)
				{
					GUI_compareAndUpdateTemperature();
				}

				if(currentTipTemperature == desiredTipTemperature)
				{
					TipTemperature = desiredTipTemperature;
					guiTempMonMode = MODE_STABLE;
					BUZZER_SetBuzzerPeriod(300);
				}
			}
			break;

			case MODE_STABLE:
			{
				int32_t maxDelta = 10;
				int32_t delta = currentTipTemperature - desiredTipTemperature;

				if(delta > maxDelta || delta < -maxDelta)
				{
					guiTempMonMode = MODE_DISRUPTION;
				}
				else
				{
					TipTemperature = desiredTipTemperature;
				}
			}
			break;

			case MODE_DISRUPTION:
			{
				if(internalCounter == 0)
				{
					if(TipTemperature < currentTipTemperature) TipTemperature++;
					else if(TipTemperature > currentTipTemperature) TipTemperature--;
					else if(TipTemperature == currentTipTemperature)
					{
						guiTempMonMode = MODE_STABILIZING;
					}
				}
			}
			break;
		}
	}
	else
	{
		if(errorOccurenceCounter < 10)
		{
			errorOccurenceCounter++;
		}
		else if (errorOccurenceCounter == 10)
		{
			BUZZER_SetBuzzerPeriod(500);
			GUI_changeCurrentViewMode(GUI_MODE_ERROR);
			guiThereWasFaultFlag = 1;
			errorOccurenceCounter++;
		}
	}
}

void GUI_Service(void)
{
	if(gui_mode_updated == 0)
	{
		switch(gui_mode)
		{
			case GUI_MODE_ERROR:
				lcd_display_registerCurrentDisplayFunc(GUI_ErrorModeView);
			break;
			case GUI_MODE_STARTUP:
				lcd_display_registerCurrentDisplayFunc(GUI_StartUpModeView);
			break;

			case GUI_MODE_NORMAL:
				lcd_display_registerCurrentDisplayFunc(GUI_NormalModeView);
			break;

			case GUI_MODE_TEMP_SET:
				lcd_display_registerCurrentDisplayFunc(GUI_TempSetModeView);
			break;

			case GUI_MODE_MENU:
				lcd_display_registerCurrentDisplayFunc(GUI_MenuModeView);
			break;

			case GUI_MODE_SLEEP:
				lcd_display_registerCurrentDisplayFunc(GUI_SleepModeView);
			break;

		}
		NVM_GetLCDParams(&LCDParams);
		gui_mode_updated = 1;
	}

	if( gui_mode == GUI_MODE_MENU )
	{
		int32_t encValue = ENCODER_GetValue();

		if(encValue == 1) gui_button = GUI_BUTTON_UP;
		else if(encValue == -1) gui_button = GUI_BUTTON_DOWN;

		ENCODER_SetValue(0);

		switch (gui_button)
		{

			case GUI_BUTTON_ACTION:
				MENU_Action();
				break;
			case GUI_BUTTON_UP:
				MENU_NextPos();
				break;
			case GUI_BUTTON_DOWN:
				MENU_PrevPos();
				break;
			case GUI_BUTTON_MENU:
			{
					MENU_BACK_STATE_t temp = MENU_BACK_PARENT_NOT_PRESENT;

					temp = MENU_BackPos();

					if(temp == MENU_BACK_PARENT_NOT_PRESENT)
					{
						ENCODER_SetValueRange(enc.minValue, enc.curretValue, enc.maxValue);
						ENCODER_DisablePostionMonitoring();
						GUI_changeCurrentViewMode( GUI_MODE_NORMAL );
						MENU_init(&MAIN_MENU);
					}
					break;
			}
			default:
				break;
		}

		gui_button = GUI_BUTTON_NO_BUTTON;
	}

	else if( gui_mode == GUI_MODE_NORMAL || gui_mode == GUI_MODE_SLEEP )
	{
		static uint8_t SLEEP_STATE = 0;
		static int32_t tempDesiredTipTemperature = 0;

		switch (gui_button)
		{

			case GUI_BUTTON_ACTION:

				break;
			case GUI_BUTTON_UP:

				if(gui_mode != GUI_MODE_SLEEP)
				{
					NVM_GetTemperatureParams(&temperatureParams);
					GUI_setNewTipTemperature(temperatureParams.savedTemperature_M1);
					m1_or_m2 = GUI_TEMPERATURE_M1;
				}

				break;

			case GUI_BUTTON_DOWN:

				if(gui_mode != GUI_MODE_SLEEP)
				{
					NVM_GetTemperatureParams(&temperatureParams);
					GUI_setNewTipTemperature(temperatureParams.savedTemperature_M2);
					m1_or_m2 = GUI_TEMPERATURE_M2;
				}



				break;

			case GUI_BUTTON_MENU:

				break;

			case GUI_BUTTON_SLEEP:
			{
				SLEEP_STATE ^= 1;

				ENCODER_GetAllParams(&enc);

				if(SLEEP_STATE)
				{
					tempDesiredTipTemperature = desiredTipTemperature;
					GUI_setNewTipTemperature(enc.minValue);
					GUI_changeCurrentViewMode(GUI_MODE_SLEEP);
					DesiredBrightness = 5;
				}
				else
				{
					GUI_setNewTipTemperature(tempDesiredTipTemperature);
					GUI_changeCurrentViewMode(GUI_MODE_NORMAL);
					DesiredBrightness = LCDParams.backlightBrightness;
				}
			}
			break;
			default:
				break;
		}

		gui_button = GUI_BUTTON_NO_BUTTON;
	}
}

void GUI_changeCurrentViewMode( GUI_MODE_t mode )
{
	gui_mode = mode;
	gui_mode_updated = 0;
}

void GUI_updateCurrentTipTemp(int32_t currentTipTemp)
{
	currentTipTemperature = currentTipTemp;
}

void GUI_updateDesiredTipTemp(int32_t desiredTipTemp)
{
	prevDesiredTipTemperature = desiredTipTemperature;
	desiredTipTemperature = desiredTipTemp;
}

void GUI_updateActualCurrentTipTemp(int32_t ActualCurrentTipTemp)
{
	currentTipTemperatureActual = ActualCurrentTipTemp;
}

void GUI_updateCurrentChipStatus(MAX31855_ChipStatus_t chipStatus, uint8_t fault_flag)
{
	guiChipStatus = chipStatus;
	guiFaultFlag = fault_flag;
}

