/*
 * gui.h
 *
 *  Created on: 28.03.2019
 *      Author: rafal
 */

#ifndef GUI_GUI_H_
#define GUI_GUI_H_

#include "../../PERIPHS/MAX31855/max31855.h"
#define STARTUP_SCREEN_TIME 50

typedef enum
{
	GUI_MODE_STARTUP,
	GUI_MODE_NORMAL,
	GUI_MODE_TEMP_SET,
	GUI_MODE_MENU,
	GUI_MODE_ERROR,
	GUI_MODE_SLEEP,
}GUI_MODE_t;

typedef enum
{
	GUI_BUTTON_NO_BUTTON,
	GUI_BUTTON_MENU,
	GUI_BUTTON_ACTION,
	GUI_BUTTON_UP,
	GUI_BUTTON_DOWN,
	GUI_BUTTON_SLEEP
}GUI_BUTTON_t;

void GUI_Init(void);
void GUI_Service(void);
void GUI_changeCurrentViewMode( GUI_MODE_t mode );
void GUI_updateActualCurrentTipTemp(int32_t ActualCurrentTipTemp);
void GUI_updateCurrentTipTemp(int32_t currentTipTemp);
void GUI_updateDesiredTipTemp(int32_t desiredTipTemp);
void GUI_TemperatureMonitoringTask(void);
void GUI_updateCurrentChipStatus(MAX31855_ChipStatus_t chipStatus, uint8_t fault_flag);

#endif /* GUI_GUI_H_ */
