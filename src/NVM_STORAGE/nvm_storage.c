/*
 * nvm_storage.c
 *
 *  Created on: 08.04.2019
 *      Author: rafal
 */

#include <stm32f0xx.h>
#include <string.h>

#include "nvm_storage.h"
#include <FEE/fee.h>
#include <global_default_tuning_values.h>

typedef struct
{
	NVM_TemperatureParams_t temperature;
	NVM_LCDParams_t			lcd;
	NVM_EncoderParams_t 	encoder;
	NVM_BuzzerParams_t		buzzer;
	NVM_LedParams_t			led;
	NVM_GuiParams_t			gui;
}NVM_AllParams_t;

#define GLOB_TUNE_TAB_SIZE (sizeof(NVM_AllParams_t)/sizeof(uint16_t))

typedef union
{
	NVM_AllParams_t tuneData;
	uint16_t tuneDataTab[GLOB_TUNE_TAB_SIZE];
}NVM_AllTuneData_u;

const NVM_AllTuneData_u defaultTuneData __attribute__((section (".text"))) =
{
	.tuneData =
	{
		.temperature =
		{
			.savedTemperature_M1   = SAVED_TIP_TEMPERATURE_M1,
			.savedTemperature_M2   = SAVED_TIP_TEMPERATURE_M2,
			.minimumTipTemperature = MINIMUM_TIP_TEMPERATURE,
			.maximumTipTemperature = MAXIMUM_TIP_TEMPERATURE,
			.startUpMode = NVM_TEMPERATURE_STARTUP_DEFAULT,
		},

		.lcd =
		{
		    .backlightBrightness = LCD_BACKLIGHT_BRIGHTNESS,
		    .contrast 			 = LCD_CONTRAST,
		},

		.encoder =
		{
			.encoderType     = ENCODER_MODE_NORMAL,
			.encoderStepVale = ENCODER_STEP_VALUE,
		},

		.buzzer =
		{
			.buzzerOnOffSwitch = BUZZER_DEFAULT_STATE,
		},

		.led =
		{
			.ledOnOffSwitch = LED_DEFAULT_STATE,
			.ledBlinkTime   = LED_DEFAULT_BLINK_TIME,
		},
		.gui =
		{
			.UpDownButtonModes = GUI_UP_DOWN_BUTTONS_DEFAULT_MODE,
		}
	}
};

NVM_AllTuneData_u allData;

static uint16_t NVM_CalcPtrIndex( void * ptr )
{
	uint32_t idx = (uint32_t) ptr;
	idx = idx - ((uint32_t)(&allData.tuneDataTab[0]));
	idx >>= 1;
	return (uint16_t)idx;
}

static void NVM_CompareAndWrite(void * local_ptr, void * source_ptr)
{
	uint16_t tmpLoc, tmpSrc;
	FEE_Var_t tmp = { 0 };

	tmpLoc = *((uint16_t *)local_ptr);
	tmpSrc = *((uint16_t *)source_ptr);

	if(tmpLoc != tmpSrc)
	{
		*((uint16_t *)local_ptr) = tmpSrc;
		tmp.varAddress = NVM_CalcPtrIndex(local_ptr);
		tmp.varValue = allData.tuneDataTab[tmp.varAddress];
		FEE_WriteVariable(&tmp);
	}
}

void NVM_Init(void)
{

	//FEE_EraseAllPages();

	FEE_Var_t temp = { 0 };

	FEE_init(GLOB_TUNE_TAB_SIZE);

	memcpy(&allData, &defaultTuneData, sizeof(NVM_AllParams_t));

	for(uint8_t i = 0; i < GLOB_TUNE_TAB_SIZE; i++)
	{
		temp.varAddress = NVM_CalcPtrIndex(&allData.tuneDataTab[i]);

		if( FEE_ReadVariable(&temp) == FEE_VAR_READ_OK)
		{
			allData.tuneDataTab[i] = temp.varValue;
		}
	}
}

void NVM_SetTemperatureParams( NVM_TemperatureParams_t * temperatureParams )
{
	NVM_TemperatureParams_t * tmp = &allData.tuneData.temperature;

	NVM_CompareAndWrite(&tmp->maximumTipTemperature, &temperatureParams->maximumTipTemperature);
	NVM_CompareAndWrite(&tmp->minimumTipTemperature, &temperatureParams->minimumTipTemperature);
	NVM_CompareAndWrite(&tmp->savedTemperature_M1, &temperatureParams->savedTemperature_M1);
	NVM_CompareAndWrite(&tmp->savedTemperature_M2, &temperatureParams->savedTemperature_M2);
}

void NVM_SetLCDParams(NVM_LCDParams_t * LCDParams)
{
	NVM_LCDParams_t * tmp = &allData.tuneData.lcd;

	NVM_CompareAndWrite(&tmp->backlightBrightness, &LCDParams->backlightBrightness);
	NVM_CompareAndWrite(&tmp->contrast, &LCDParams->contrast);
}


void NVM_SetEncoderParams( NVM_EncoderParams_t * encoderParams)
{
	NVM_EncoderParams_t * tmp = &allData.tuneData.encoder;

	NVM_CompareAndWrite(&tmp->encoderStepVale, &encoderParams->encoderStepVale);
	NVM_CompareAndWrite(&tmp->encoderType, &encoderParams->encoderType);
}


void NVM_SetBuzzerParams( NVM_BuzzerParams_t  * SetBuzzerParams )
{
	NVM_BuzzerParams_t  * tmp = &allData.tuneData.buzzer;
	NVM_CompareAndWrite(&tmp->buzzerOnOffSwitch, &SetBuzzerParams->buzzerOnOffSwitch);
}


void NVM_SetLedParams(NVM_LedParams_t * ledParams)
{
	NVM_LedParams_t * tmp = &allData.tuneData.led;
	NVM_CompareAndWrite(&tmp->ledOnOffSwitch, &ledParams->ledOnOffSwitch);
	NVM_CompareAndWrite(&tmp->ledBlinkTime, &ledParams->ledBlinkTime);
}

void NVM_SetGUIParams(NVM_GuiParams_t * guiParams)
{
	NVM_GuiParams_t * tmp = &allData.tuneData.gui;
	NVM_CompareAndWrite(&tmp->UpDownButtonModes, &guiParams->UpDownButtonModes);
}



void NVM_GetTemperatureParams( NVM_TemperatureParams_t * temperatureParams)
{
	memcpy(temperatureParams, &allData.tuneData.temperature, sizeof(NVM_TemperatureParams_t));
}


void NVM_GetLCDParams(NVM_LCDParams_t * LCDParams)
{
	memcpy(LCDParams, &allData.tuneData.lcd, sizeof(NVM_LCDParams_t));
}


void NVM_GetEncoderParams(NVM_EncoderParams_t * encoderParams)
{
	memcpy(encoderParams, &allData.tuneData.encoder, sizeof(NVM_EncoderParams_t));
}


void NVM_GetBuzzerParams(NVM_BuzzerParams_t  * buzzerParams )
{
	memcpy(buzzerParams, &allData.tuneData.buzzer, sizeof(NVM_BuzzerParams_t));
}


void NVM_GetLedParams(NVM_LedParams_t * ledParams)
{
	memcpy(ledParams, &allData.tuneData.led, sizeof(NVM_LedParams_t));
}

void NVM_GetGUIParams(NVM_GuiParams_t * GUIParams)
{
	memcpy(GUIParams, &allData.tuneData.gui, sizeof(NVM_GuiParams_t));
}




