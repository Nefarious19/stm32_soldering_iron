/*
 * nvm_storage.h
 *
 *  Created on: 08.04.2019
 *      Author: rafal
 */

#ifndef NVM_STORAGE_NVM_STORAGE_H_
#define NVM_STORAGE_NVM_STORAGE_H_

#include <tim1.h>
#include <stdint.h>

typedef enum
{
	NVM_TEMPERATURE_STARTUP_DEFAULT,
	NVM_TEMPERATURE_STARTUP_M1,
	NVM_TEMPERATURE_STARTUP_M2
} NVM_TemperatureStartUpMode_e;

typedef struct
{
	uint16_t savedTemperature_M1;
	uint16_t savedTemperature_M2;
	uint16_t minimumTipTemperature;
	uint16_t maximumTipTemperature;
	NVM_TemperatureStartUpMode_e startUpMode;
} NVM_TemperatureParams_t;

typedef struct
{
	uint16_t backlightBrightness;
	uint16_t contrast;
}NVM_LCDParams_t;

typedef struct
{
	ENCODER_TYPE_t encoderType;
	int16_t encoderStepVale;
}NVM_EncoderParams_t;

typedef struct
{
	uint16_t buzzerOnOffSwitch;
}NVM_BuzzerParams_t;

typedef struct
{
	uint16_t ledOnOffSwitch;
	uint16_t ledBlinkTime;
}NVM_LedParams_t;

typedef struct
{
	uint16_t UpDownButtonModes;
}NVM_GuiParams_t;

void NVM_Init(void);

void NVM_SetTemperatureParams( NVM_TemperatureParams_t * temperatureParams );
void NVM_SetLCDParams(NVM_LCDParams_t * LCDParams);
void NVM_SetEncoderParams( NVM_EncoderParams_t * encoderParams);
void NVM_SetBuzzerParams(NVM_BuzzerParams_t  * SetBuzzerParams );
void NVM_SetLedParams(NVM_LedParams_t * ledParams);
void NVM_SetGUIParams(NVM_GuiParams_t * guiParams);

void NVM_GetTemperatureParams( NVM_TemperatureParams_t * temperatureParams);
void NVM_GetLCDParams(NVM_LCDParams_t * LCDParams);
void NVM_GetEncoderParams( NVM_EncoderParams_t * encoderParams);
void NVM_GetBuzzerParams(NVM_BuzzerParams_t  * buzzerParams );
void NVM_GetLedParams(NVM_LedParams_t * ledParams);
void NVM_GetGUIParams(NVM_GuiParams_t * GUIParams);


#endif /* NVM_STORAGE_NVM_STORAGE_H_ */
