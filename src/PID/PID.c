/*
 * PID.c
 *
 *  Created on: 06.03.2019
 *      Author: rafal
 */
#include "stm32f0xx.h"
#include <stdint.h>
#include "PID.h"

static PID_t pid = { 0 };

void PID_Init(int32_t Kp, int32_t Ki, int32_t Kd, int32_t maxOutputValue, int32_t minOutputValue, int32_t scalingFactor )
{
	pid.scalingFactor = scalingFactor;
	pid.Kp = Kp;
	pid.Ki = Ki;
	pid.Kd = Kd;
	pid.maxOutputValue = maxOutputValue;
	pid.minOutputValue = minOutputValue;
	pid.maxITermValue = pid.maxOutputValue * scalingFactor;
	pid.minITermValue = pid.minOutputValue * scalingFactor;
	pid.minPTermValue = pid.maxOutputValue * scalingFactor *(-1);
	pid.maxPTermValue = pid.maxOutputValue * scalingFactor;
	pid.currentErrorValue = 0;
	pid.lastProcessValue = 0;
	pid.integralValue = 0;
	pid.lastErrorValue = 0;
}

void PID_reset(void)
{
	pid.currentErrorValue = 0;
	pid.lastProcessValue = 0;
	pid.integralValue = 0;
	pid.lastErrorValue = 0;
}

int32_t PID_Calc(int32_t setOutputObjectValue, int32_t currentObjectOutputValue)
{
	int32_t P_term;
	int32_t D_term;
	int32_t I_term;
	int32_t Output;

	pid.currentErrorValue = setOutputObjectValue - currentObjectOutputValue;

	pid.integralValue += pid.currentErrorValue;
	I_term = pid.Ki * pid.integralValue;

	if(I_term > pid.maxITermValue)
	{
		I_term = pid.maxITermValue;
		pid.integralValue = pid.maxITermValue / pid.Ki;
	}
	else if(I_term < pid.minITermValue)
	{
		I_term = pid.minITermValue;
		pid.integralValue = pid.minITermValue / pid.Ki;
	}

	P_term = pid.Kp * pid.currentErrorValue;

	if(P_term > pid.maxPTermValue)
	{
		P_term = pid.maxPTermValue;
	}
	else if(P_term < pid.minPTermValue)
	{
		P_term = pid.minPTermValue;
	}


	D_term =  currentObjectOutputValue - pid.lastProcessValue;
	D_term *= pid.Kd;

	if(D_term < 0) D_term = 0;

	pid.lastProcessValue = currentObjectOutputValue;

	Output = (P_term + I_term - D_term) / pid.scalingFactor;

	if(Output > pid.maxOutputValue)
	{
		Output = pid.maxOutputValue;
	}
	else if(Output < pid.minOutputValue)
	{
		Output = pid.minOutputValue;
	}

	return Output;
}
