/*
 * PID.h
 *
 *  Created on: 06.03.2019
 *      Author: rafal
 */

#ifndef PID_H_
#define PID_H_

#define SCALING_FACTOR 10000

typedef struct
{
	int32_t Kp;
	int32_t Ki;
	int32_t Kd;
	int32_t maxOutputValue;
	int32_t minOutputValue;
	int32_t currentErrorValue;
	int32_t lastProcessValue;
	int32_t lastErrorValue;
	int32_t maxITermValue;
	int32_t minITermValue;
	int32_t maxIntegralValue;
	int32_t minIntegralValue;
	int32_t maxPTermValue;
	int32_t minPTermValue;
	int32_t integralValue;
	int32_t scalingFactor;
}PID_t;

void PID_Init(int32_t Kp, int32_t Ki, int32_t Kd, int32_t maxOutputValue, int32_t minOutputValue, int32_t scalingFactor );
int32_t PID_Calc(int32_t setOutputObjectValue, int32_t currentObjectOutputValue);
void PID_reset(void);

#endif /* PID_H_ */
