/*
 * task_pid.h
 *
 *  Created on: 27.03.2019
 *      Author: rafal
 */

#ifndef PID_TASK_TASK_PID_H_
#define PID_TASK_TASK_PID_H_

void task_pid_main(void);
void task_pid_updateCurrentTipTemperature(int32_t curTipTemperature);
void task_pid_updateDesiredTipTemperature(int32_t desTipTemperature);
void task_pid_Enable(void);
void task_pid_Disable(void);



#endif /* PID_TASK_TASK_PID_H_ */
