/*
 * task_pid.c
 *
 *  Created on: 27.03.2019
 *      Author: rafal
 */

#include "stm32f0xx.h"
#include "../../PERIPHS/TRIAC_CONTROL/triac.h"
#include "../../PERIPHS/ENCODER/encoder.h"
#include "../PID/PID.h"

static int32_t currentTipTemperature = 0;
static int32_t desiredTipTemperature = 0;
static uint8_t PID_STOP = 0;

void task_pid_main(void)
{
	if(PID_STOP)
	{
		int32_t ret = 0;
		ret = PID_Calc(desiredTipTemperature, currentTipTemperature);
		TRIAC_setPower(ret);
	}
	else
	{
		TRIAC_setPower(0);
	}
}


void task_pid_updateCurrentTipTemperature(int32_t curTipTemperature)
{
	currentTipTemperature = curTipTemperature;
}

void task_pid_updateDesiredTipTemperature(int32_t desTipTemperature)
{
	desiredTipTemperature = desTipTemperature;
}

void task_pid_Enable(void)
{
	PID_STOP = 1;
}

void task_pid_Disable(void)
{
	PID_STOP = 0;
}
