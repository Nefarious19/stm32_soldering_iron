/*
 * menu_def.h
 *
 *  Created on: 18.04.2019
 *      Author: rafal
 */

#ifndef MENU_MENU_DEF_H_
#define MENU_MENU_DEF_H_



typedef enum
{
	MENU_KEY_MODE_NAVIGATION,
	MENU_KEY_MODE_FUNCTION
}MENU_KEYS_MODE_t;

typedef enum
{
	MENU_KEY_NO_KEY,
	MENU_KEY_UP,
	MENU_KEY_DOWN,
	MENU_KEY_ACTION,
	MENU_KEY_BACK
}MENU_KEYS_t;

typedef int ( * functionPointer)( MENU_KEYS_t key_pressed );

struct menu_node
{
	struct menu_node * parent;
	struct menu_node * child;
	struct menu_node * next_node;
	struct menu_node * prev_node;
	const char * node_name;
	functionPointer funcPtr;
};

struct menu_node MAIN_MENU;


#endif /* MENU_MENU_DEF_H_ */
