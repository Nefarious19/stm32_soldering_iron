/*
 * menu.c
 *
 *  Created on: 18.04.2019
 *      Author: rafal
 */


#include "menu.h"
#include "menu_def.h"

#include "../PERIPHS/ST7032/st7032.h"

#include <string.h>

static struct menu_node * first_node;
static struct menu_node * actual_node;
static struct menu_node * actual_subnode;

static uint8_t menuLevel = 0;

static MENU_KEYS_MODE_t menu_keyMode = MENU_KEY_MODE_NAVIGATION;
static MENU_KEYS_t menu_key = MENU_KEY_NO_KEY;

void MENU_init(struct menu_node * first_node_ptr)
{
	if(first_node_ptr != 0)
	{
		first_node = first_node_ptr;
		actual_node = first_node_ptr;
		actual_subnode = actual_node->child;
		menuLevel = 0;
		menu_keyMode = MENU_KEY_MODE_NAVIGATION;
	}
}

static void MENU_ClrRow(uint8_t y)
{
	STLCD_BUF_locate(0,y);
	STLCD_BUF_puts("                ");
	STLCD_BUF_locate(0,y);
}

static void MENU_RefreshNode(void)
{
	MENU_ClrRow(0);
	STLCD_BUF_puts(actual_node->node_name);
}

static void MENU_RefreshSubnode(void)
{
	MENU_ClrRow(1);
	STLCD_BUF_puts("> ");
	STLCD_BUF_puts(actual_subnode->node_name);
}

void MENU_Show(void)
{
	STLCD_BUF_refresh();
}

void MENU_NextPos(void)
{
	if(menu_keyMode == MENU_KEY_MODE_NAVIGATION)
	{
		if(actual_subnode->next_node != 0)
		{
			actual_subnode = actual_subnode->next_node;
			MENU_RefreshSubnode();
		}
	}
	else if(menu_keyMode == MENU_KEY_MODE_FUNCTION)
	{
		menu_key = MENU_KEY_UP;
		actual_node->funcPtr(menu_key);
	}

}

void MENU_PrevPos(void)
{
	if(menu_keyMode == MENU_KEY_MODE_NAVIGATION)
	{
		if(actual_subnode->prev_node != 0)
		{
			actual_subnode = actual_subnode->prev_node;
			MENU_RefreshSubnode();
		}
	}
	else if(menu_keyMode == MENU_KEY_MODE_FUNCTION)
	{
		menu_key = MENU_KEY_DOWN;
		actual_node->funcPtr(menu_key);
	}
}

MENU_BACK_STATE_t MENU_BackPos(void)
{
	MENU_BACK_STATE_t retVal;

	if(actual_node->parent != 0)
	{
		actual_subnode = actual_node;
		actual_node = actual_node->parent;
		menuLevel--;
		MENU_RefreshNode();
		MENU_RefreshSubnode();
		retVal = MENU_BACK_PARENT_PRESENT;
		if(menu_keyMode == MENU_KEY_MODE_FUNCTION)
		{
			menu_keyMode = MENU_KEY_MODE_NAVIGATION;
		}
	}
	else
	{
		retVal = MENU_BACK_PARENT_NOT_PRESENT;
	}
	return retVal;
}

void MENU_Action(void)
{
	if(menuLevel == 0)
	{
		menuLevel++;
		MENU_RefreshNode();
		MENU_RefreshSubnode();
	}
	else
	{
		if(menu_keyMode == MENU_KEY_MODE_NAVIGATION)
		{
			if(actual_subnode->child != 0)
			{
				actual_node = actual_subnode;
				actual_subnode = actual_node->child;
				MENU_RefreshNode();
				MENU_RefreshSubnode();
				menuLevel++;
			}
			else if(actual_subnode->funcPtr != 0)
			{
				STLCD_BUF_locate( strlen(actual_node->node_name), 0);
				STLCD_BUF_puts(" > ");
				STLCD_BUF_puts(actual_subnode->node_name);

				actual_node = actual_subnode;
				actual_subnode = 0;
				menu_key = MENU_KEY_NO_KEY;
				actual_node->funcPtr(menu_key);
				menu_keyMode = MENU_KEY_MODE_FUNCTION;
			}
		}
		else if(menu_keyMode == MENU_KEY_MODE_FUNCTION)
		{
			menu_key = MENU_KEY_ACTION;
			if(actual_node->funcPtr(menu_key))
			{

				menu_keyMode = MENU_KEY_MODE_NAVIGATION;
				actual_subnode = actual_node;
				actual_node = actual_node->parent;
				MENU_RefreshNode();
				MENU_RefreshSubnode();

			}

		}
	}
}

