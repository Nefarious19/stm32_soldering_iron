/*
 * menu.h
 *
 *  Created on: 18.04.2019
 *      Author: rafal
 */

#ifndef MENU_MENU_H_
#define MENU_MENU_H_

#include <stm32f0xx.h>

#include "menu_def.h"

typedef enum
{
	MENU_BACK_PARENT_PRESENT,
	MENU_BACK_PARENT_NOT_PRESENT
}MENU_BACK_STATE_t;

void MENU_init(struct menu_node * first_node_ptr);

void MENU_Show(void);
void MENU_NextPos(void);
void MENU_PrevPos(void);
MENU_BACK_STATE_t MENU_BackPos(void);

void MENU_Action(void);

#endif /* MENU_MENU_H_ */
