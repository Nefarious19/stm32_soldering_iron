/*
 * mendu_def.c
 *
 *  Created on: 18.04.2019
 *      Author: rafal
 */

#include "menu_def.h"
#include "../NVM_STORAGE/nvm_storage.h"
#include "../../PERIPHS/ST7032/st7032.h"
#include "../../UTILS/PROG_TIMERS/prog_timers.h"

#include "../../PERIPHS/LED/led.h"
#include "../../PERIPHS/MAX31855/max31855.h"
#include "../../PERIPHS/BUZZER/buzzer.h"
#include "../../PERIPHS/ST7032/st7032.h"
#include "../../PERIPHS/TRIAC_CONTROL/triac.h"
#include "../../PERIPHS/ENCODER/encoder.h"
#include <tim1.h>

#include "../PID_TASK/task_pid.h"
#include "../LCD_DISPLAY_TASK/lcd_display_task.h"
#include "../CALCULATE_TEMP_TASK/calculate_temp_task.h"
#include "../LED_TASK/led_task.h"
#include "../GUI/gui.h"

static void MENU_ClrRow(uint8_t y)
{
	STLCD_BUF_locate(0,y);
	STLCD_BUF_puts("                ");
	STLCD_BUF_locate(0,y);
}

struct menu_node LED_MENU;
	struct menu_node LED_MENU_STATE;
	struct menu_node LED_MENU_BLINK_TIME;

	int LED_MENU_STATE_func(MENU_KEYS_t key);
	int LED_MENU_BLINK_TIME_func(MENU_KEYS_t key);

struct menu_node BUZZER_MENU;
	struct menu_node BUZZER_MENU_STATE;
	int BUZZER_MENU_STATE_func(MENU_KEYS_t key);

struct menu_node ENCODER_MENU;
	struct menu_node ENCODER_MENU_TYPE;
	struct menu_node ENCODER_MENU_STEP;
	int ENCODER_MENU_MODE_func(MENU_KEYS_t key);
	int ENCODER_MENU_STEP_func(MENU_KEYS_t key);

	struct menu_node DISPLAY_MENU;
		struct menu_node DISPLAY_MENU_BRIGHTNESS;
		struct menu_node DISPLAY_MENU_CONTRAST;
		int DISPLAY_MENU_BRIGHTNESS_func(MENU_KEYS_t key);
		int DISPLAY_MENU_CONTRAST_func(MENU_KEYS_t key);

struct menu_node SOFT_VER_MENU;
	struct menu_node SOFT_VER_MENU_SHOW;



struct menu_node MAIN_MENU    = { 0, 		  &BUZZER_MENU,  0 , 0, 			"****  MENU  ****", 0};
	struct menu_node LED_MENU = { &MAIN_MENU, &LED_MENU_STATE, &ENCODER_MENU , &BUZZER_MENU,  "LED" , 0};
		struct menu_node LED_MENU_STATE = { &LED_MENU, 0, &LED_MENU_BLINK_TIME , 0, "STATE" , LED_MENU_STATE_func};
		struct menu_node LED_MENU_BLINK_TIME = { &LED_MENU, 0, 0, &LED_MENU_STATE,  "BLINK TIME" , LED_MENU_BLINK_TIME_func};

	struct menu_node BUZZER_MENU  = { &MAIN_MENU, &BUZZER_MENU_STATE, &LED_MENU	   , 0, 		    "BUZZER" , 0};
		struct menu_node BUZZER_MENU_STATE = { &BUZZER_MENU, 0, 0, 0, "STATE", BUZZER_MENU_STATE_func};

	struct menu_node ENCODER_MENU = { &MAIN_MENU, &ENCODER_MENU_TYPE, &DISPLAY_MENU	   , &LED_MENU,     "ENCODER" , 0};
		struct menu_node ENCODER_MENU_TYPE = { &ENCODER_MENU, 0, &ENCODER_MENU_STEP, 0, "MODE", ENCODER_MENU_MODE_func };
		struct menu_node ENCODER_MENU_STEP = { &ENCODER_MENU, 0, 0, &ENCODER_MENU_TYPE, "STEPS", ENCODER_MENU_STEP_func };

	struct menu_node DISPLAY_MENU = { &MAIN_MENU, &DISPLAY_MENU_BRIGHTNESS, &SOFT_VER_MENU, &ENCODER_MENU, "DISPLAY" , 0};
		struct menu_node DISPLAY_MENU_BRIGHTNESS = {&DISPLAY_MENU, 0, &DISPLAY_MENU_CONTRAST, 0, "BRIGHT", DISPLAY_MENU_BRIGHTNESS_func};
		struct menu_node DISPLAY_MENU_CONTRAST = {&DISPLAY_MENU, 0, 0, &DISPLAY_MENU_BRIGHTNESS, "CONTRAST", DISPLAY_MENU_CONTRAST_func};

	struct menu_node SOFT_VER_MENU = { &MAIN_MENU, &SOFT_VER_MENU_SHOW, 0, &DISPLAY_MENU,"SOFT VERSION" , 0};
	struct menu_node SOFT_VER_MENU_SHOW = {&SOFT_VER_MENU, 0, 0, 0, "V1.0 23-07-19", 0};

static void LCD_DISPLAY_BAR(int32_t min, int32_t max, int32_t currVal)
{
	STLCD_BUF_locate(0,1);
	uint8_t bar_cntr = 0;
	if(currVal > max) currVal = max;

	int32_t progress = ((currVal * 5000/ (max - min)))/100;

	while(progress > 4)
	{
		STLCD_BUF_putc(5);
		progress -= 5;
		bar_cntr++;
	}

	if(progress)
	{
		STLCD_BUF_putc(progress%5);
		bar_cntr++;
	}
	else if(progress == 0 && bar_cntr == 0)
	{
		bar_cntr = 1;
	}

	for(uint8_t i = bar_cntr-1; i < 9; i++)
	{
		STLCD_BUF_putc(0);
	}
}


int LED_MENU_STATE_func(MENU_KEYS_t key)
{
	NVM_LedParams_t temp = { 0 };
	NVM_GetLedParams(&temp);
	static uint8_t LEDstate = 0;
	int retVal = 0;

	switch(key)
	{
		case MENU_KEY_NO_KEY:
			LEDstate = temp.ledOnOffSwitch;
		break;

		case MENU_KEY_UP:
			LEDstate ^= 1;
		break;

		case MENU_KEY_DOWN:
			LEDstate ^= 1;
		break;

		case MENU_KEY_ACTION:
		{
			uint8_t change = 0;

			if(temp.ledOnOffSwitch == 1 && LEDstate == 0)
			{
				PROG_TIMER_Delete(led_blink_task);
				change = 1;
			}
			else if(temp.ledOnOffSwitch == 0 && LEDstate == 1)
			{
				PROG_TIMER_Create(led_blink_task, temp.ledBlinkTime);
				change = 1;
			}

			if(change)
			{
				temp.ledOnOffSwitch = LEDstate;
				if(!LEDstate) temp.ledBlinkTime = 0;
				NVM_SetLedParams(&temp);
			}
			retVal = 1;
		}

		break;

		case MENU_KEY_BACK:
			LEDstate = temp.ledOnOffSwitch;
			retVal = 1;
		break;
	}

	MENU_ClrRow(1);
	STLCD_BUF_puts("> ");
	if(LEDstate) STLCD_BUF_puts("LED ON");
	else  STLCD_BUF_puts("LED OFF");

	return retVal;
}

int LED_MENU_BLINK_TIME_func(MENU_KEYS_t key)
{
	int retVal = 0;

	static uint16_t LED_blinkingTime = 0;

	NVM_LedParams_t temp = { 0 };

	NVM_GetLedParams(&temp);

	switch(key)
	{
		case MENU_KEY_NO_KEY:
			LED_blinkingTime = temp.ledBlinkTime;
		break;

		case MENU_KEY_UP:
			LED_blinkingTime++;
		break;

		case MENU_KEY_DOWN:
			LED_blinkingTime--;
		break;

		case MENU_KEY_ACTION:
		{
			if(LED_blinkingTime != temp.ledBlinkTime)
			{

				if( temp.ledOnOffSwitch == 0 )
				{
					temp.ledBlinkTime = LED_blinkingTime;
					temp.ledOnOffSwitch = 1;
					NVM_SetLedParams(&temp);
					PROG_TIMER_Create(led_blink_task, temp.ledBlinkTime );
				}
				else
				{
					PROG_TIMER_Delete(led_blink_task);
					temp.ledBlinkTime = LED_blinkingTime;
					temp.ledOnOffSwitch = 1;
					NVM_SetLedParams(&temp);
					PROG_TIMER_Create(led_blink_task, temp.ledBlinkTime );
				}
			}

			retVal = 1;
		}
		break;

		case MENU_KEY_BACK:
			temp.ledBlinkTime = LED_blinkingTime;
			retVal = 1;
		break;
	}
	MENU_ClrRow(1);
	if(LED_blinkingTime < 1) LED_blinkingTime = 1;
	else if(LED_blinkingTime > 999) LED_blinkingTime = 999;

	LCD_DISPLAY_BAR(0, 999, LED_blinkingTime);
	STLCD_BUF_locate(11,1);
	STLCD_BUF_putsInt(LED_blinkingTime , 10);
	STLCD_BUF_putc('m');
	STLCD_BUF_putc('s');
	return retVal;
}

int BUZZER_MENU_STATE_func(MENU_KEYS_t key)
{
	NVM_BuzzerParams_t temp = { 0 };
	NVM_GetBuzzerParams(&temp);
	static uint8_t BUZZERstate = 0;
	int retVal = 0;

	switch(key)
	{
		case MENU_KEY_NO_KEY:
			BUZZERstate = temp.buzzerOnOffSwitch;
		break;

		case MENU_KEY_UP:
			BUZZERstate ^= 1;
		break;

		case MENU_KEY_DOWN:
			BUZZERstate ^= 1;
		break;

		case MENU_KEY_ACTION:
		{
			uint8_t change = 0;

			if(temp.buzzerOnOffSwitch == 1 && BUZZERstate == 0)
			{
				BUZZER_Off();
				PROG_TIMER_Delete(BUZZER_Task);
				change = 1;
			}
			else if(temp.buzzerOnOffSwitch == 0 && BUZZERstate == 1)
			{
				PROG_TIMER_Create(BUZZER_Task,10);
				change = 1;
			}

			if(change)
			{
				temp.buzzerOnOffSwitch = BUZZERstate;
				NVM_SetBuzzerParams(&temp);
			}
			retVal = 1;
		}

		break;

		case MENU_KEY_BACK:
			BUZZERstate = temp.buzzerOnOffSwitch;
			retVal = 1;
		break;
	}

	MENU_ClrRow(1);
	STLCD_BUF_puts("> ");
	if(BUZZERstate) STLCD_BUF_puts("BUZZER ON");
	else  STLCD_BUF_puts("BUZZER OFF");

	return retVal;
}

int ENCODER_MENU_MODE_func(MENU_KEYS_t key)
{
	NVM_EncoderParams_t temp = { 0 };
	NVM_GetEncoderParams(&temp);

	static ENCODER_TYPE_t encType = ENCODER_MODE_NORMAL;

	int retVal = 0;

	switch(key)
	{
		case MENU_KEY_NO_KEY:
			encType = temp.encoderType;
		break;

		case MENU_KEY_UP:
			encType ^= 1;
		break;

		case MENU_KEY_DOWN:
			encType ^= 1;
		break;

		case MENU_KEY_ACTION:
		{
			if(temp.encoderType != encType)
			{
				temp.encoderType = encType;
				NVM_SetEncoderParams(&temp);
				TIM1_EncoderModeInit(encType);
			}
			retVal = 1;
		}

		break;

		case MENU_KEY_BACK:
			encType = temp.encoderType;
			retVal = 1;
		break;
	}

	MENU_ClrRow(1);
	STLCD_BUF_puts("> ");
	if(encType) STLCD_BUF_puts("INVERSED");
	else  STLCD_BUF_puts("NORMAL");

	return retVal;
}

int ENCODER_MENU_STEP_func(MENU_KEYS_t key)
{
	NVM_EncoderParams_t temp = { 0 };
	NVM_GetEncoderParams(&temp);

	static int16_t encStepValue = 2;

	int retVal = 0;

	switch(key)
	{
		case MENU_KEY_NO_KEY:
			encStepValue = temp.encoderStepVale;
		break;

		case MENU_KEY_UP:
			encStepValue <<= 1;
			if(encStepValue > 16) encStepValue = 16;
		break;

		case MENU_KEY_DOWN:
			encStepValue >>= 1;
			if(encStepValue < 2) encStepValue = 2;

		break;

		case MENU_KEY_ACTION:
		{
			if(encStepValue != temp.encoderStepVale)
			{
				temp.encoderStepVale = encStepValue;
				NVM_SetEncoderParams(&temp);
				ENCODER_SetStepValue(encStepValue);
			}
			retVal = 1;
		}

		break;

		case MENU_KEY_BACK:
			encStepValue = temp.encoderStepVale;
			retVal = 1;
		break;
	}

	MENU_ClrRow(1);
	STLCD_BUF_puts("STEPS NO: ");
	STLCD_BUF_putsInt(encStepValue, 10);


	return retVal;
}

int DISPLAY_MENU_BRIGHTNESS_func(MENU_KEYS_t key)
{
	int retVal = 0;
	static int16_t brightness = 0;
	NVM_LCDParams_t temp;
	NVM_GetLCDParams(&temp);

	switch(key)
	{
		case MENU_KEY_NO_KEY:
			brightness = temp.backlightBrightness;
		break;

		case MENU_KEY_UP:
			brightness++;
			STLCD_setBacklightBrigtness(brightness);

		break;

		case MENU_KEY_DOWN:
			brightness--;
			STLCD_setBacklightBrigtness(brightness);
		break;

		case MENU_KEY_ACTION:
		{
			if(brightness != temp.backlightBrightness)
			{
				temp.backlightBrightness = brightness;
				STLCD_setBacklightBrigtness(temp.backlightBrightness);
				NVM_SetLCDParams(&temp);
			}
			retVal = 1;
		}
		break;

		case MENU_KEY_BACK:
			temp.backlightBrightness = brightness;
			retVal = 1;
		break;
	}
	MENU_ClrRow(1);
	if(brightness < 1) brightness = 1;
	else if(brightness > 100) brightness = 100;

	LCD_DISPLAY_BAR(0, 100, brightness);
	STLCD_BUF_locate(11,1);
	STLCD_BUF_putsInt(brightness , 10);
	STLCD_BUF_putc('%');
	return retVal;
}

int DISPLAY_MENU_CONTRAST_func(MENU_KEYS_t key)
{
	int retVal = 0;
	static int16_t contrast = 0;
	NVM_LCDParams_t temp;
	NVM_GetLCDParams(&temp);

	switch(key)
	{
		case MENU_KEY_NO_KEY:
			contrast = temp.contrast;
		break;

		case MENU_KEY_UP:
			contrast++;
		break;

		case MENU_KEY_DOWN:
			contrast--;
		break;

		case MENU_KEY_ACTION:
		{
			if(contrast != temp.contrast)
			{
				temp.contrast = contrast;
				NVM_SetLCDParams(&temp);
			}
			retVal = 1;
		}
		break;

		case MENU_KEY_BACK:
			temp.contrast = contrast;
			retVal = 1;
		break;
	}
	MENU_ClrRow(1);
	if(contrast < 1) contrast = 1;
	else if(contrast > 100) contrast = 100;
	STLCD_setContrast(contrast);
	LCD_DISPLAY_BAR(0, 0x0F, contrast);
	STLCD_BUF_locate(11,1);
	STLCD_BUF_putsInt(contrast , 10);
	STLCD_BUF_putc('%');
	return retVal;
}
