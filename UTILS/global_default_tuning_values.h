/*
 * global_default_tuning_values.h
 *
 *  Created on: 14.04.2019
 *      Author: rafal
 */

#ifndef GLOBAL_DEFAULT_TUNING_VALUES_H_
#define GLOBAL_DEFAULT_TUNING_VALUES_H_

#include <tim1.h>

/***************************************************
 * TEMPERATURE TUNING DEFAULT VALUES
 ***************************************************/
#define SAVED_TIP_TEMPERATURE_M1 	320
#define SAVED_TIP_TEMPERATURE_M2 	350
#define MINIMUM_TIP_TEMPERATURE 	80
#define MAXIMUM_TIP_TEMPERATURE 	480

/***************************************************
 * LCD TUNING DEFAULT VALUES
 ***************************************************/
#define LCD_BACKLIGHT_BRIGHTNESS	80
#define LCD_CONTRAST			 	06

/***************************************************
 * ENCODER TUNING DEFAULT VALUES
 ***************************************************/
#define ENCODER_TYPE				ENCODER_MODE_INVERSED
#define ENCODER_STEP_VALUE			4

/***************************************************
 * ENCODER TUNING DEFAULT VALUES
 ***************************************************/
#define BUZZER_DEFAULT_STATE        1

/***************************************************
 * LED TUNING DEFAULT VALUES
 ***************************************************/
#define LED_DEFAULT_STATE			1
#define LED_DEFAULT_BLINK_TIME		100

/***************************************************
 * GUI TUNING DEFAULT VALUES
 ***************************************************/
#define GUI_UP_DOWN_BUTTONS_DEFAULT_MODE 1

#endif /* GLOBAL_DEFAULT_TUNING_VALUES_H_ */
