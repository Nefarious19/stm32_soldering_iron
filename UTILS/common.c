/*
 * common.c
 *
 *  Created on: 26.02.2019
 *      Author: rafal
 */

#include "stm32f0xx.h"

void initSystemClock(void)
{
	RCC->CR |= RCC_CR_HSEON;
	while(!(RCC->CR & RCC_CR_HSERDY));
	RCC->CFGR2 |= RCC_CFGR2_PREDIV_DIV2;
	RCC->CFGR |= RCC_CFGR_PLLSRC_HSE_PREDIV;
	RCC->CFGR |= RCC_CFGR_PLLMUL6;
	RCC->CR |= RCC_CR_PLLON;
	while(!(RCC->CR & RCC_CR_PLLRDY));
	FLASH->ACR |= FLASH_ACR_LATENCY;
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	while(!((RCC->CFGR & RCC_CFGR_SWS_Msk) == RCC_CFGR_SWS_PLL));
}

void initGpioClocks(void)
{
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
}
