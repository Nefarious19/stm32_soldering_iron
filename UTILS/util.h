/*
 * util.h
 *
 *  Created on: 24.02.2019
 *      Author: rafal
 */

#ifndef UTIL_H_
#define UTIL_H_

#include <stm32f0xx.h>

#define ATOMIC_BLOCK( x )\
{\
	volatile register uint32_t STATUS = __get_PRIMASK(); \
	__disable_irq(); 									 \
	x;													 \
	__set_PRIMASK(STATUS);								 \
	__enable_irq();                                      \
}




#endif /* UTIL_H_ */
