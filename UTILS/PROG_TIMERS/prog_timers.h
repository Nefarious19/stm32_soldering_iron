/*
 * prog_timers.h
 *
 *  Created on: 06.03.2019
 *      Author: rafal
 */

#ifndef PROG_TIMERS_H_
#define PROG_TIMERS_H_

void PROG_TIMER_Create(void(*timer_handler_ptr)(void), uint32_t period);
void PROG_TIMER_Delete(void(*timer_handler_ptr)(void));
void PROG_TIMER_Service(void);

#endif /* PROG_TIMERS_H_ */
