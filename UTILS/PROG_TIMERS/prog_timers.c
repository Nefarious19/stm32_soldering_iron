/*
 * prog_timers.c
 *
 *  Created on: 06.03.2019
 *      Author: rafal
 */

#include "stm32f0xx.h"

#include <string.h>
#include <malloc.h>

#include "prog_timers.h"
#include "../util.h"

struct PROG_TIMER_t
{
	void(*func_handler_ptr)(void);
	uint32_t prog_timer_period;
	volatile uint32_t current_prog_timer_value;
	struct PROG_TIMER_t * next_prog_timer_ptr;
};

static struct PROG_TIMER_t * first = 0;

void SysTick_Handler(void)
{
	struct PROG_TIMER_t * current = 0;

	if(first != 0)
	{
		current = first;

		while(current)
		{
			if(current->current_prog_timer_value) current->current_prog_timer_value--;
			current = current->next_prog_timer_ptr;
		}
	}
}

void PROG_TIMER_Service(void)
{
	struct PROG_TIMER_t * current = 0;

	if(first != 0)
	{
		current = first;

		while(current)
		{
			if(!current->current_prog_timer_value)
			{
				current->current_prog_timer_value = current->prog_timer_period;
				if(current->func_handler_ptr)current->func_handler_ptr();
			}

			current = current->next_prog_timer_ptr;
		}
	}
}


void PROG_TIMER_Create(void(*timer_handler_ptr)(void), uint32_t period)
{
	struct PROG_TIMER_t * tmp = 0;
	struct PROG_TIMER_t * tmp1 = 0;

	tmp = malloc(sizeof(struct PROG_TIMER_t));
	tmp->current_prog_timer_value = period;
	tmp->prog_timer_period = period;
	tmp->func_handler_ptr = timer_handler_ptr;
	tmp->next_prog_timer_ptr = 0;

	if(first == 0)
	{
		first = tmp;
		return;
	}
	else
	{
		tmp1 = first;

		while(tmp1->next_prog_timer_ptr != 0)
		{
			tmp1 = tmp1->next_prog_timer_ptr;
		}

		tmp1->next_prog_timer_ptr = tmp;
	}
}

void PROG_TIMER_Delete(void(*timer_handler_ptr)(void))
{
	struct PROG_TIMER_t * tmp = first;
	struct PROG_TIMER_t * prev_tmp = first;
	uint8_t prog_timer_found = 0;

	for(;;)
	{
		if( (tmp != 0) && (tmp->func_handler_ptr == timer_handler_ptr) )
		{
			prog_timer_found = 1;
			break;
		}
		else if( tmp != 0 )
		{
			prev_tmp = tmp;
			tmp = tmp->next_prog_timer_ptr;
		}
		else
		{
			break;
		}
	}

	if(prog_timer_found)
	{
		ATOMIC_BLOCK
		(
			{
				prev_tmp->next_prog_timer_ptr = tmp->next_prog_timer_ptr;
				free(tmp);
			}
		)
	}
}
