/*
 * fee.c
 *
 *  Created on: 02.04.2019
 *      Author: rafal
 */


#include "stm32f0xx.h"

#include "fee.h"

static const uint32_t FEE_PagesAdresses[FEE_NUM_OF_USED_PAGES] =
{
	FEE_PAGE_1_BEGIN_ADDRESS,
	FEE_PAGE_2_BEGIN_ADDRESS,
	FEE_PAGE_3_BEGIN_ADDRESS,
	FEE_PAGE_4_BEGIN_ADDRESS
};

FEE_PageStatus_t FEE_StateOfPages[FEE_NUM_OF_USED_PAGES];

FEE_CurrentActivePage_t FEE_ActivePage;

uint32_t FEE_MaximumVirtualAddress = 0;

static void FEE_FlashUnlock(void)
{
	while ((FLASH->SR & FLASH_SR_BSY) != 0)
	{

	}
	if ((FLASH->CR & FLASH_CR_LOCK) != 0)
	{
		FLASH->KEYR = FEE_KEY1;
		FLASH->KEYR = FEE_KEY2;
	}
}

static void FEE_FlashLock(void)
{
	FLASH->CR |= FLASH_CR_LOCK;
}

static void FEE_ProgramHalfWord(uint32_t addr, uint16_t data)
{
	FLASH->CR |= FLASH_CR_PG;

	*(__IO uint16_t*)(addr) = data;

	while ((FLASH->SR & FLASH_SR_BSY) != 0)
	{

	}

	if ((FLASH->SR & FLASH_SR_EOP) != 0)
	{
		FLASH->SR = FLASH_SR_EOP;
	}
	else
	{

	}

	FLASH->CR &= ~FLASH_CR_PG;
}

static void FEE_PageErase(uint32_t page_addr)
{
	FEE_FlashUnlock();

	FLASH->CR |= FLASH_CR_PER;
	FLASH->AR = page_addr;
	FLASH->CR |= FLASH_CR_STRT;

	while ((FLASH->SR & FLASH_SR_BSY) != 0)
	{

	}

	if ((FLASH->SR & FLASH_SR_EOP) != 0)
	{
		FLASH->SR = FLASH_SR_EOP;
	}
	else
	{

	}

	FLASH->CR &= ~FLASH_CR_PER;

	FEE_FlashLock();
}

static void FEE_WriteUnderEmptyField(FEE_Var_t * var)
{
	uint32_t write_address = 0;
	uint32_t * flash_ptr = 0;
	write_address = FEE_PagesAdresses[FEE_ActivePage] + 4;

	while(write_address < (FEE_PagesAdresses[FEE_ActivePage] + FEE_PAGE_SIZE) )
	{
		flash_ptr = (uint32_t *)write_address;

		if( *flash_ptr == 0xFFFFFFFF )
		{
			break;
		}
		else
		{
			write_address += 4;
		}
	}

	FEE_FlashUnlock();

	FEE_ProgramHalfWord(write_address,  var->varAddress);
	FEE_ProgramHalfWord(write_address+2, var->varValue);

	FEE_FlashLock();
}

static void FEE_PageRewrite(uint32_t src_address)
{
	uint32_t srcAddrIdx = 0;
	FEE_Var_t tmp = { 0 };

	for(int16_t addr = 0; addr > FEE_MaximumVirtualAddress ; addr++)
	{
		srcAddrIdx = src_address + FEE_PAGE_SIZE - 4;

		for(uint16_t i = 0; i < ((FEE_PAGE_SIZE/4)-1); i++)
		{
			tmp.varAddress = *(__IO uint16_t*)(srcAddrIdx);

			if(tmp.varAddress == addr)
			{
				tmp.varValue = *(__IO uint16_t*)(srcAddrIdx+2);
				FEE_WriteUnderEmptyField(&tmp);
				break;
			}
			else
			{
				srcAddrIdx -= 4;
			}
		}
	}
}

static void FEE_UpdatePageStates(void)
{
	//Get info about state of each page
	for(uint8_t page = 0; page < FEE_NUM_OF_USED_PAGES; page++)
	{
		FEE_StateOfPages[page] = (FEE_PageStatus_t) *((uint32_t * )(FEE_PagesAdresses[page]));
	}
}

void FEE_EraseAllPages(void)
{
	for(uint8_t i = 0; i < FEE_NUM_OF_USED_PAGES; i++)
	{
		FEE_PageErase(FEE_PagesAdresses[i]);
	}
}

void FEE_init(uint32_t maximumVirtualAddress)
{
	uint8_t status = 0;

	FEE_MaximumVirtualAddress = maximumVirtualAddress - 1;

	FEE_UpdatePageStates();

	do
	{
		for(uint8_t page = 0; page < FEE_NUM_OF_USED_PAGES; page++)
		{
			if( (FEE_StateOfPages[page] != FEE_PAGE_ERASED) && (FEE_StateOfPages[page] != FEE_PAGE_VALID) )
			{
				uint8_t tmpPageIdx = page + 1;
				if( tmpPageIdx > (FEE_NUM_OF_USED_PAGES - 1) ) tmpPageIdx = 0;

				if( (FEE_StateOfPages[tmpPageIdx] == FEE_PAGE_ERASED) || (FEE_StateOfPages[tmpPageIdx] == FEE_PAGE_VALID) )
				{
					FEE_ActivePage = (FEE_CurrentActivePage_t)(tmpPageIdx);
					FEE_PageRewrite(FEE_PagesAdresses[page]);
				}
				else
				{
					while(tmpPageIdx != page)
					{
						tmpPageIdx++;
						if( tmpPageIdx > (FEE_NUM_OF_USED_PAGES - 1) ) tmpPageIdx = 0;

						if( (FEE_StateOfPages[tmpPageIdx] == FEE_PAGE_ERASED) || (FEE_StateOfPages[tmpPageIdx] == FEE_PAGE_VALID) )
						{
							FEE_ActivePage = (FEE_CurrentActivePage_t)(tmpPageIdx);
							FEE_PageRewrite(FEE_PagesAdresses[page]);
							status = 1;
							break;
						}
						else
						{
							FEE_PageErase(FEE_PagesAdresses[tmpPageIdx]);
						}
					}
				}
				status = 1;
				break;
			}
		}

		if(status) break;

		for(uint8_t page = 0; page < FEE_NUM_OF_USED_PAGES; page++)
		{
			if(FEE_StateOfPages[page] == FEE_PAGE_VALID)
			{
				FEE_ActivePage = (FEE_CurrentActivePage_t)(page);
				status = 1;
				break;
			}
		}

		if(status) break;

		for(uint8_t page = 0; page < FEE_NUM_OF_USED_PAGES; page++)
		{
			if(FEE_StateOfPages[page] == FEE_PAGE_ERASED)
			{
				FEE_ActivePage = (FEE_CurrentActivePage_t)(page);
				status = 1;
				break;
			}
		}

		if(status) break;

	}while(0);

}

void FEE_WriteVariable(FEE_Var_t * var)
{
	uint32_t write_address = 0;
	write_address = FEE_PagesAdresses[FEE_ActivePage] + 4;

	uint8_t emptyFieldNotFound = 1;

	uint8_t prevPageIdx = FEE_ActivePage;
	uint8_t nextPageIdx = FEE_ActivePage + 1;
	if( nextPageIdx > (FEE_NUM_OF_USED_PAGES - 1) ) nextPageIdx = 0;

	while(write_address < (FEE_PagesAdresses[FEE_ActivePage] + FEE_PAGE_SIZE) )
	{
		if( *((uint32_t *)write_address) == 0xFFFFFFFF )
		{
			emptyFieldNotFound = 0;
			break;
		}
		else
		{
			write_address += 4;
		}
	}

	if( emptyFieldNotFound )
	{
		FEE_ActivePage = nextPageIdx;

		if( *(__IO uint16_t*)(FEE_PagesAdresses[FEE_ActivePage]) != FEE_PAGE_ERASED)
		{
			FEE_PageErase(FEE_PagesAdresses[FEE_ActivePage]);
		}

		FEE_FlashUnlock();
		FEE_ProgramHalfWord(FEE_PagesAdresses[prevPageIdx],FEE_PAGE_FULL);
		FEE_FlashLock();

		FEE_PageRewrite(FEE_PagesAdresses[prevPageIdx]);
		FEE_PageErase(FEE_PagesAdresses[prevPageIdx]);
	}

	FEE_WriteUnderEmptyField(var);

	if( *(__IO uint16_t*)(FEE_PagesAdresses[FEE_ActivePage]) == FEE_PAGE_ERASED)
	{
		FEE_FlashUnlock();
		FEE_ProgramHalfWord(FEE_PagesAdresses[FEE_ActivePage],FEE_PAGE_VALID);
		FEE_FlashLock();
	}
}

FEE_ReadStatus_t FEE_ReadVariable(FEE_Var_t * ptr)
{
	uint32_t srcAddrIdx = 0;
	uint32_t lastAddrIdx = 0;
	uint16_t tmpVirtualAddres = ptr->varAddress;
	uint16_t tmp = 0;
	FEE_ReadStatus_t retVal = FEE_VAR_READ_ERROR;
	srcAddrIdx = FEE_PagesAdresses[FEE_ActivePage] + FEE_PAGE_SIZE - 4;
	lastAddrIdx = FEE_PagesAdresses[FEE_ActivePage] + 4;

	while(srcAddrIdx != lastAddrIdx)
	{
		tmp = *(__IO uint16_t*)(srcAddrIdx);

		if(tmp == tmpVirtualAddres)
		{
			ptr->varValue = *(__IO uint16_t*)(srcAddrIdx+2);
			retVal = FEE_VAR_READ_OK;
			break;
		}
		else
		{
			srcAddrIdx -= 4;
		}
	}

	return retVal;
}
