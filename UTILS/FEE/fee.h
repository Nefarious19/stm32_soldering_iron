/*
 * fee.h
 *
 *  Created on: 02.04.2019
 *      Author: rafal
 */

#ifndef FEE_FEE_H_
#define FEE_FEE_H_

#define FEE_KEY1  0x45670123
#define FEE_KEY2  0xCDEF89AB

#define FEE_NUM_OF_USED_PAGES 		4
#define FEE_PAGE_SIZE 				1024
#define FEE_LASTPAGE_ADDRESS 		0x08007C00

#define FEE_PAGE_1_BEGIN_ADDRESS	(FEE_LASTPAGE_ADDRESS - (3*FEE_PAGE_SIZE))
#define FEE_PAGE_2_BEGIN_ADDRESS	(FEE_LASTPAGE_ADDRESS - (2*FEE_PAGE_SIZE))
#define FEE_PAGE_3_BEGIN_ADDRESS	(FEE_LASTPAGE_ADDRESS - (1*FEE_PAGE_SIZE))
#define FEE_PAGE_4_BEGIN_ADDRESS	(FEE_LASTPAGE_ADDRESS - (0*FEE_PAGE_SIZE))

#define FEE_PAGE_1_END_ADDRESS		FEE_PAGE_1_BEGIN_ADDRESS + FEE_PAGE_SIZE - 1
#define FEE_PAGE_2_END_ADDRESS		FEE_PAGE_2_BEGIN_ADDRESS + FEE_PAGE_SIZE - 1
#define FEE_PAGE_3_END_ADDRESS		FEE_PAGE_3_BEGIN_ADDRESS + FEE_PAGE_SIZE - 1
#define FEE_PAGE_4_END_ADDRESS		FEE_PAGE_4_BEGIN_ADDRESS + FEE_PAGE_SIZE - 1

typedef struct
{
	uint16_t varAddress;
	uint16_t varValue;
}FEE_Var_t;

typedef enum
{
	FEE_PAGE_ERASED 	= 0xFFFF,
	FEE_PAGE_VALID  	= 0xFF00,
	FEE_PAGE_FULL   	= 0x0000
}FEE_PageStatus_t;

typedef enum
{
	FEE_ACTIVE_PAGE_0,
	FEE_ACTIVE_PAGE_1,
	FEE_ACTIVE_PAGE_2,
	FEE_ACTIVE_PAGE_3,
	FEE_NO_VALID_PAGE
}FEE_CurrentActivePage_t;

typedef enum
{
	FEE_VAR_READ_OK,
	FEE_VAR_READ_ERROR
}FEE_ReadStatus_t;

void FEE_init(uint32_t maximumVirtualAddress);
void FEE_WriteVariable(FEE_Var_t * var);
FEE_ReadStatus_t FEE_ReadVariable(FEE_Var_t * ptr);
void FEE_EraseAllPages(void);

#endif /* FEE_FEE_H_ */
