/*
 * common.h
 *
 *  Created on: 26.02.2019
 *      Author: rafal
 */

#ifndef COMMON_H_
#define COMMON_H_

void initSystemClock(void);
void initGpioClocks(void);

#endif /* COMMON_H_ */
