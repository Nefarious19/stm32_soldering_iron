/*
 * buzzer.c
 *
 *  Created on: 26.02.2019
 *      Author: rafal
 */

#include "../../PERIPHS/BUZZER/buzzer.h"



static uint8_t BUZZER_Enabled = 1;
static uint16_t BUZZER_ON_period = 0;
static uint8_t BUZZER_StateUpdated = 0;

void BUZZER_Init(uint8_t buzzerState)
{
	BUZZER_Enabled = buzzerState;

	if(buzzerState == 1)
	{
		GPIOB->MODER |= GPIO_MODER_MODER1_0;
		GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR1;
		GPIOB->ODR &= ~(1<<1);
	}
	else
	{
		GPIOB->MODER &= ~GPIO_MODER_MODER0;
		GPIOB->OSPEEDR &= ~GPIO_OSPEEDER_OSPEEDR1;
		GPIOB->ODR &= ~(1<<1);
	}
}

void BUZZER_Task(void)
{
	if(BUZZER_ON_period)
	{
		if(BUZZER_StateUpdated)
		{
			BUZZER_On();
			BUZZER_StateUpdated = 0;
		}
		BUZZER_ON_period--;
		if(!BUZZER_ON_period)
		{
			BUZZER_StateUpdated = 1;
		}
	}
	else if(BUZZER_StateUpdated)
	{
		BUZZER_Off();
		BUZZER_StateUpdated = 0;
	}

}

void BUZZER_SetBuzzerPeriod(uint16_t periodMS)
{
	BUZZER_ON_period = periodMS / 10;
	BUZZER_StateUpdated = 1;
}

void BUZZER_On(void)
{
	if(BUZZER_Enabled) GPIOB->ODR |= (1<<1);
}

void BUZZER_Off(void)
{
	if(BUZZER_Enabled) GPIOB->ODR &= ~(1<<1);
}

void BUZZER_Tog(void)
{
	if(BUZZER_Enabled) GPIOB->ODR ^= (1<<1);
}
