/*
 * buzzer.h
 *
 *  Created on: 26.02.2019
 *      Author: rafal
 */

#ifndef BUZZER_BUZZER_H_
#define BUZZER_BUZZER_H_

#include "stm32f0xx.h"

void BUZZER_Init(uint8_t buzzerState);
void BUZZER_Task(void);
void BUZZER_SetBuzzerPeriod(uint16_t periodMS);
void BUZZER_On(void);
void BUZZER_Off(void);
void BUZZER_Tog(void);
#endif /* BUZZER_BUZZER_H_ */
