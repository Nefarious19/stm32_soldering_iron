/*
 * st7032.c
 *
 *  Created on: 02.02.2019
 *      Author: rafal
 */

#include "../../PERIPHS/ST7032/st7032.h"

#include <stm32f0xx.h>
#include <tim3.h>
#include <stdlib.h>
#include <string.h>
#include <i2c.h>


#define ST7032_CONTROL_BYTE_POS 0
#define ST7032_DATA_BYTE_POS 	1
#define ST7032_CLR_BYTE        ' '

#if (ST7032_USE_BUFFER == 1)

static char st7032_lcd_buffer[ST7032_HEIGHT][ST7032_WIDTH];

typedef struct
{
	uint8_t x;
	uint8_t y;
}ST7032_curPos_t;

static ST7032_curPos_t pos =
{
	.x = 0,
	.y = 0
};

#endif

static uint8_t st7032_i2c_tab[2];

#define STLCD_LastControlByte()     st7032_i2c_tab[ST7032_CONTROL_BYTE_POS] &= ~(1<<7);
#define STLCD_NOT_LastControlByte() st7032_i2c_tab[ST7032_CONTROL_BYTE_POS] |= (1<<7);

#define STLCD_CommandMode()     st7032_i2c_tab[ST7032_CONTROL_BYTE_POS] &= ~(1<<6);
#define STLCD_DataMode()        st7032_i2c_tab[ST7032_CONTROL_BYTE_POS] |= (1<<6);

static void STLCD_delay(void)
{
	for(uint32_t i = 0; i < 130; i++)
	{
		asm volatile("NOP");
	}
}

static void STLCD_delay_4ms(void)
{
	for( uint8_t i = 0; i < 200; i++ )
	{
		STLCD_delay();
	}
}
static inline void STLCD_sendI2C(uint8_t data)
{
	st7032_i2c_tab[ST7032_DATA_BYTE_POS] = data;
	I2C_writeNBytes(ST7032_I2C_ADDRESS, st7032_i2c_tab,2);
}

static void STLCD_sendCmd(uint8_t cmd)
{
	STLCD_LastControlByte();
	STLCD_CommandMode();
	STLCD_sendI2C(cmd);
	STLCD_delay();
}

static void STLCD_sendData(uint8_t data)
{
	STLCD_LastControlByte();
	STLCD_DataMode();
	STLCD_sendI2C(data);
}



void STLCD_setBacklightBrigtness(uint8_t brightness)
{
	TIM3_PWMSetDutyCycle(brightness);
}

void STLCD_setContrast(uint8_t contrast)
{
	STLCD_sendCmd(ST_FUNCTION                 |
				  ST_FUNCTION_INTERFACE_8BIT  |
				  ST_FUNCTION_NO_OF_LINES_EQ_2|
				  ST_FUNCTION_EXT_INTRUCTIONS_ON);

	STLCD_sendCmd(ST_SET_INTERNAL_OSC_FREQ    			|
				  ST_SET_INTERNAL_OSC_FREQ_BIAS_1_5		|
				  (ST_SET_INTERNAL_OSC_FREQ_MASK & 0x04));

	STLCD_sendCmd(ST_CONTRAST_SET | (contrast & 0x0F) );

	STLCD_sendCmd(ST_BOOSTER_ICON_CONTRAST_SET 			 |
				  ST_BOOSTER_ICON_CONTRAST_SET_EN_BOOSTER);

	STLCD_sendCmd(ST_SET_FOLLOWER             |
				  ST_SET_FOLLOWER_FOLLOWER_ON |
				  0x07                        );

	STLCD_sendCmd(ST_FUNCTION                 |
				  ST_FUNCTION_INTERFACE_8BIT  |
				  ST_FUNCTION_NO_OF_LINES_EQ_2|
				  ST_FUNCTION_EXT_INTRUCTIONS_OFF);
}

void STLCD_init(void)
{
	TIM3_PWMInit();

	GPIOB->MODER |= GPIO_MODER_MODER0_0;
	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR0;
	GPIOB->ODR &= ~(1<<0);

	STLCD_delay();

	GPIOB->ODR |= (1<<0);

	for(uint32_t i = 0; i < 2000; i++)
	{
		STLCD_delay();
	}

	STLCD_sendCmd(ST_FUNCTION                 |
				  ST_FUNCTION_INTERFACE_8BIT  |
			      ST_FUNCTION_NO_OF_LINES_EQ_2);

	STLCD_sendCmd(ST_FUNCTION                 |
				  ST_FUNCTION_INTERFACE_8BIT  |
				  ST_FUNCTION_NO_OF_LINES_EQ_2|
				  ST_FUNCTION_EXT_INTRUCTIONS_ON);

	STLCD_sendCmd(ST_SET_INTERNAL_OSC_FREQ    			|
				  ST_SET_INTERNAL_OSC_FREQ_BIAS_1_5		|
				  (ST_SET_INTERNAL_OSC_FREQ_MASK & 0x04));

	STLCD_sendCmd(ST_CONTRAST_SET | 0x06);

	STLCD_sendCmd(ST_BOOSTER_ICON_CONTRAST_SET 			 |
				  ST_BOOSTER_ICON_CONTRAST_SET_EN_BOOSTER);

	STLCD_sendCmd(ST_SET_FOLLOWER             |
			      ST_SET_FOLLOWER_FOLLOWER_ON |
			      0x07                        );

	for(uint32_t i = 0; i < 2000; i++)
	{
		STLCD_delay();
	}

	STLCD_sendCmd(ST_DISPLAY_ON                |
				  ST_DSIPLAY_ON_DIPLAY_ON      |
				  ST_DSIPLAY_ON_CURSOR_OFF	   |
				  ST_DSIPLAY_ON_CURSOR_POS_OFF );

	STLCD_sendCmd(ST_CLR_DISPLAY);
	STLCD_delay_4ms();

#if (ST7032_USE_BUFFER == 1)
	STLCD_BUF_clr();
#endif

	STLCD_sendCmd(ST_FUNCTION                 |
				  ST_FUNCTION_INTERFACE_8BIT  |
				  ST_FUNCTION_NO_OF_LINES_EQ_2|
				  ST_FUNCTION_EXT_INTRUCTIONS_OFF);
}


void STLCD_locate(uint8_t x, uint8_t y)
{
	STLCD_LastControlByte();
	STLCD_CommandMode();
	switch(y)
	{
		case 0:
			STLCD_sendCmd(ST_SET_DDRAM_ADDRESS | (ST7032_LINE1_ADDRESS + x) );
		break;
		case 1:
			STLCD_sendCmd(ST_SET_DDRAM_ADDRESS | (ST7032_LINE2_ADDRESS + x) );
		break;
	}
}

void STLCD_clr(void)
{
	STLCD_sendCmd(ST_CLR_DISPLAY);
	STLCD_locate(0,0);
}

void STLCD_putc(char data)
{
	STLCD_sendData(data);
}

void STLCD_puts(char * data)
{
	char ch = 0;
	while((ch = *data++))
	{
		STLCD_putc(ch);
	}
}

void STLCD_putsInt(int32_t number, uint8_t BASE)
{
	char * ptr = 0;
	ptr = malloc(16);
	memset(ptr,0,16);
	__itoa(number,ptr,BASE);
	STLCD_puts(ptr);
	free(ptr);
}

void STLCD_setCGRAMPattern(uint8_t idx, const uint8_t * data)
{
	STLCD_sendCmd(ST_SET_CGRAM_ADDRESS| (idx<<3));

	for(uint8_t i = 0; i < 8; i++)
	{
		STLCD_sendData(data[i]);
	}
}

#if (ST7032_USE_BUFFER == 1)

void STLCD_BUF_locate(uint8_t x, uint8_t y)
{
	pos.x = x;
	pos.y = y;
}

void STLCD_BUF_clr(void)
{
	pos.x = 0;
	pos.y = 0;
	memset(st7032_lcd_buffer[0], ST7032_CLR_BYTE, ST7032_WIDTH);
	memset(st7032_lcd_buffer[1], ST7032_CLR_BYTE, ST7032_WIDTH);
}

void STLCD_BUF_putc(char data)
{
	st7032_lcd_buffer[pos.y][pos.x] = data;

	pos.x++;

	if(pos.x == ST7032_WIDTH)
	{
		pos.x = 0;
	}
}

void STLCD_BUF_puts(const char * data)
{
	char ch = 0;
	while((ch = *data++))
	{
		STLCD_BUF_putc(ch);
	}
}

void STLCD_BUF_putsInt(int32_t number, uint8_t BASE)
{
	char * ptr = 0;
	ptr = malloc(16);
	memset(ptr,0,16);
	__itoa(number,ptr,BASE);
	STLCD_BUF_puts(ptr);
	free(ptr);
}

void STLCD_BUF_refresh(void)
{
	uint8_t i = 0, j = 0;
	char * ptr = 0;

	for(i = 0; i < ST7032_HEIGHT; i++)
	{
		STLCD_locate(0, i);
		ptr = st7032_lcd_buffer[i];
		for(j = 0; j < ST7032_WIDTH; j++)
		{
			STLCD_sendData(*ptr++);
		}
	}
}

#endif
