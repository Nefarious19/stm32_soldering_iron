/*
 * st7032.h
 *
 *  Created on: 02.02.2019
 *      Author: rafal
 */

#ifndef ST7032_H_
#define ST7032_H_

#include <stdint.h>

/*
 * CONFIGURATION
 */
#define ST7032_I2C_ADDRESS 		0x3E
#define ST7032_USE_BUFFER 		1
#define ST7032_WIDTH			16
#define ST7032_HEIGHT			2
/*
 * DDRAM ADRESSES
 */
#define ST7032_LINE1_ADDRESS 0x00
#define ST7032_LINE2_ADDRESS 0x40

/*
 * COMMANDS
 */

#define ST_CLR_DISPLAY                    		0x01

#define ST_RETURN_HOME 					  		0x02

#define ST_SET_ENTRY_MODE                 		0x04
	#define ST_ENTRY_MODE_CUR_INC         			0x02
	#define ST_ENTRY_MODE_CUR_DEC         			0x00
    #define ST_ENTRY_MODE_SHIFT_RIGHT     			0x00
	#define ST_ENTRY_MODE_SHIFT_LEFT      			0x01

#define ST_DISPLAY_ON	                  		0x08
    #define ST_DSIPLAY_ON_DIPLAY_ON       			0x04
	#define ST_DSIPLAY_ON_DIPLAY_OFF      			0x00
    #define ST_DSIPLAY_ON_CURSOR_ON       			0x02
	#define ST_DSIPLAY_ON_CURSOR_OFF  	  			0x00
    #define ST_DSIPLAY_ON_CURSOR_POS_ON   			0x01
	#define ST_DSIPLAY_ON_CURSOR_POS_OFF  			0x00

#define ST_FUNCTION 					      	0x20
	#define ST_FUNCTION_INTERFACE_8BIT				0x10
	#define ST_FUNCTION_INTERFACE_4BIT				0x00
	#define ST_FUNCTION_NO_OF_LINES_EQ_1      		0x00
	#define ST_FUNCTION_NO_OF_LINES_EQ_2      		0x08
	#define ST_FUNCTION_FONT_SIZE_5x8         		0x00
	#define ST_FUNCTION_FONT_SIZE_5x16       	    0x04
	#define ST_FUNCTION_EXT_INTRUCTIONS_ON    		0x01
	#define ST_FUNCTION_EXT_INTRUCTIONS_OFF   		0x00

#define ST_SET_DDRAM_ADDRESS                    0x80

#define ST_SET_CGRAM_ADDRESS                    0x40

#define ST_SET_CURSOR_DISPLAY_SHIFT             0x10
	#define ST_SET_CURSOR_DISPLAY_SHIFT_DISPLAY     0x08
	#define ST_SET_CURSOR_DISPLAY_SHIFT_CURSOR      0x00
	#define ST_SET_CURSOR_DISPLAY_SHIFT_INC_RIGHT   0x04
	#define ST_SET_CURSOR_DISPLAY_SHIFT_INC_LEFT    0x00

//extended commands
#define ST_SET_INTERNAL_OSC_FREQ                0x10
	#define ST_SET_INTERNAL_OSC_FREQ_BIAS_1_4     	0x08
	#define ST_SET_INTERNAL_OSC_FREQ_BIAS_1_5     	0x00
	#define ST_SET_INTERNAL_OSC_FREQ_MASK         	0x07

#define ST_SET_ICON_ADDRESS 					0x40

#define ST_BOOSTER_ICON_CONTRAST_SET			0x50
#define ST_BOOSTER_ICON_CONTRAST_SET_EN_C4			0x01
#define ST_BOOSTER_ICON_CONTRAST_SET_DIS_C4         0x00
#define ST_BOOSTER_ICON_CONTRAST_SET_EN_C5          0x02
#define ST_BOOSTER_ICON_CONTRAST_SET_DIS_C5         0x00
#define ST_BOOSTER_ICON_CONTRAST_SET_EN_ICON        0x08
#define ST_BOOSTER_ICON_CONTRAST_SET_DIS_ICON       0x00
#define ST_BOOSTER_ICON_CONTRAST_SET_EN_BOOSTER     0x04
#define ST_BOOSTER_ICON_CONTRAST_SET_DIS_BOOSTER    0x00

#define ST_SET_FOLLOWER 						0x60
	#define ST_SET_FOLLOWER_FOLLOWER_ON				0x08
	#define ST_SET_FOLLOWER_FOLLOWER_OFF			0x00

#define ST_CONTRAST_SET 						0x70


void STLCD_init(void);

void STLCD_locate(uint8_t x, uint8_t y);
void STLCD_putc(char data);
void STLCD_puts(char * data);
void STLCD_clr(void);
void STLCD_putsInt(int32_t number, uint8_t BASE);
void STLCD_setBacklightBrigtness(uint8_t brightness);
void STLCD_setContrast(uint8_t contrast);
void STLCD_setCGRAMPattern(uint8_t idx, const uint8_t * data);

#if (ST7032_USE_BUFFER == 1)
	void STLCD_BUF_locate(uint8_t x, uint8_t y);
	void STLCD_BUF_clr(void);
	void STLCD_BUF_putc(char data);
	void STLCD_BUF_puts(const char * data);
	void STLCD_BUF_putsInt(int32_t number, uint8_t BASE);
	void STLCD_BUF_refresh(void);
#endif
#endif /* ST7032_H_ */
