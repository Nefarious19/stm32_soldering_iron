/*
 * encoder.h
 *
 *  Created on: 03.03.2019
 *      Author: rafal
 */

#ifndef ENCODER_ENCODER_H_
#define ENCODER_ENCODER_H_

typedef enum
{
	ENCODER_BUTTON_RELEASED,
	ENCODER_BUTTON_PRESSED,
	ENCODER_BUTTON_HELD,
} ENCODER_ButtonState_t;

typedef enum
{
	ENCODER_POSTION_MONITORING_ENABLE,
	ENCODER_POSTION_MONITORING_DISABLE,
}ENCODER_PosMon_te;

typedef struct
{
	int32_t				minValue;
	int32_t				maxValue;
	int32_t 			curretValue;
	int32_t 			mulValue;
	int32_t 			stepValue;
	ENCODER_PosMon_te 	postionMonitoringEnable;
	ENCODER_ButtonState_t buttonState;
}ENCODER_t;

void ENCODER_Init(int32_t stepValue);
void ENCODER_RegisterButtonPressedCallback(void(*func_ptr)(void));
void ENCODER_Service(void);
void ENCODER_SetValueRange(int32_t min, int32_t curVal, int32_t max);
void ENCODER_SetValue(int32_t curVal);
void ENCODER_SetStepValue(int32_t stepValue);
int32_t ENCODER_GetValue(void);
void ENCODER_EnablePostionMonitoring(void);
void ENCODER_DisablePostionMonitoring(void);
void ENCODER_SetMulValue(int32_t mulVal);
void ENCODER_GetAllParams(ENCODER_t * encPtr);

#endif /* ENCODER_ENCODER_H_ */
