/*
 * encoder.c
 *
 *  Created on: 03.03.2019
 *      Author: rafal
 */

#include <tim1.h>
#include <ext_int.h>
#include <string.h>

#include "stm32f0xx.h"
#include "encoder.h"

#define ENCODER_TIM1_RESET_VALUE (0xFFFF / 2)



static ENCODER_t enc =
{
	.minValue = 0x80000001,
	.maxValue = 0x7FFFFFFE,
	.curretValue = 0x00000000,
	.postionMonitoringEnable = ENCODER_POSTION_MONITORING_DISABLE,
	.buttonState = ENCODER_BUTTON_RELEASED
};

void ( * ButtonPressedCallback )(void) = (void (*)) 0;

void ENCODER_ButtonPressedHandler(void);

void ENCODER_Init(int32_t stepValue)
{
	EXT_INT_EXT10_RegsiterHandler(ENCODER_ButtonPressedHandler, EXT_INT_INTERRUPT_DEBOUNCE);

	enc.mulValue = 1;
	enc.stepValue = stepValue;
}

void ENCODER_RegisterButtonPressedCallback(void(*func_ptr)(void))
{
	if(func_ptr) ButtonPressedCallback = func_ptr;
}

void ENCODER_SetMulValue(int32_t mulVal)
{
	enc.mulValue = mulVal;
}

void ENCODER_SetStepValue(int32_t stepValue)
{
	enc.stepValue = stepValue;
}

void ENCODER_Service(void)
{
	static int16_t lastEncoderValue = 0;

	int16_t tmpEncValue = 0;
	int16_t tmp = 0;

	if(enc.postionMonitoringEnable == ENCODER_POSTION_MONITORING_ENABLE)
	{
		tmpEncValue = (int16_t)(TIM1->CNT);

		if( tmpEncValue < (int16_t)(0x8000 + 4))
		{
			tmpEncValue = 0;
			lastEncoderValue = 0;
			TIM1->CNT = 0;
		}

		tmp = tmpEncValue - lastEncoderValue;

		if( tmp >=  enc.stepValue )
		{
			enc.curretValue += ( 1 * enc.mulValue);

			if( enc.curretValue > enc.maxValue )
			{
				enc.curretValue = enc.maxValue;
			}

			lastEncoderValue = tmpEncValue;
		}
		else if( tmp <= -enc.stepValue)
		{
			enc.curretValue -= ( 1 * enc.mulValue);

			if( enc.curretValue < enc.minValue )
			{
				enc.curretValue = enc.minValue;
			}

			lastEncoderValue = tmpEncValue;
		}
	}
	else if(enc.postionMonitoringEnable == ENCODER_POSTION_MONITORING_DISABLE)
	{
		tmpEncValue = (int16_t)(TIM1->CNT);
		if(tmpEncValue != lastEncoderValue)
		{
			lastEncoderValue = tmpEncValue;
		}
	}

	if(enc.buttonState == ENCODER_BUTTON_PRESSED)
	{
		if(ButtonPressedCallback)
		{
			ButtonPressedCallback();
		}
		enc.buttonState = ENCODER_BUTTON_HELD;
	}
	else if(enc.buttonState == ENCODER_BUTTON_HELD)
	{
		if( !(GPIOA->IDR & ~(1<<10)))
		{
			enc.buttonState == ENCODER_BUTTON_HELD;
		}
		else
		{
			enc.buttonState == ENCODER_BUTTON_RELEASED;
		}
	}
}

void ENCODER_SetValueRange(int32_t min, int32_t curVal, int32_t max)
{
	if( min >= max ) return;

	enc.curretValue = curVal;
	enc.maxValue = max;
	enc.minValue = min;
	enc.mulValue = 1;
}

void ENCODER_SetValue(int32_t curVal)
{
	if( curVal < enc.minValue )
	{
		enc.curretValue = enc.minValue;
	}
	else if( curVal > enc.maxValue )
	{
		enc.curretValue = enc.maxValue;
	}
	else
	{
		enc.curretValue = curVal;
	}
}

int32_t ENCODER_GetValue(void)
{
	return enc.curretValue;
}

void ENCODER_EnablePostionMonitoring(void)
{
	enc.postionMonitoringEnable = ENCODER_POSTION_MONITORING_ENABLE;
}

void ENCODER_DisablePostionMonitoring(void)
{
	enc.postionMonitoringEnable = ENCODER_POSTION_MONITORING_DISABLE;
}

void ENCODER_ButtonPressedHandler(void)
{
	enc.buttonState = ENCODER_BUTTON_PRESSED;
}

void ENCODER_GetAllParams(ENCODER_t * encPtr)
{
	memcpy(encPtr, &enc, sizeof(ENCODER_t));
}
