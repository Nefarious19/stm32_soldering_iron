/*
 * triac.h
 *
 *  Created on: 04.02.2019
 *      Author: rafal
 */

#ifndef TRIAC_H_
#define TRIAC_H_

void TRIAC_init(void);
void TRIAC_setPower(uint8_t power);
void TRIAC_beginNewCycle(void);
uint8_t TRIAC_isCycleUpdated(void);
void TRIAC_Service(void);
void TRIAC_RegisterFullCycleEndCallback( void( * func_ptr)( void ) );

#endif /* TRIAC_H_ */
