/*
 * triac.c
 *
 *  Created on: 04.02.2019
 *      Author: rafal
 */


#include "stm32f0xx.h"
#include "triac.h"
#include <ext_int.h>
#include <tim16.h>
static volatile uint8_t heaterPower = 0;
static volatile uint8_t cycleUpdated = 0;

void TRIAC_zeroCrossEventHandler(void);
void TRIAC_powerControlRoutine(void);

void ( * TRIAC_fullCycleEndCallback)(void) = ( void * )(0);

static inline void TRIAC_on(void)
{
	GPIOA->ODR |=  (1<<12);
}

static inline void TRIAC_off(void)
{
	GPIOA->ODR &= ~(1<<12);
}

void TRIAC_init(void)
{
 	//TRIAC init
	GPIOA->MODER |= GPIO_MODER_MODER12_0;
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR12;
	GPIOA->ODR &= ~(1<<12);

	TIM16_init();
	TIM16_RegisterTimer10msCallback(TRIAC_powerControlRoutine);
	EXT_INT_EXT11_RegsiterHandler(TRIAC_zeroCrossEventHandler, EXT_INT_INTERRUPT_DIRECT);
}

void TRIAC_Service(void)
{
	if( (cycleUpdated == 1) && (TRIAC_fullCycleEndCallback != 0) )
	{
		TRIAC_fullCycleEndCallback();
		cycleUpdated = 0;
	}
}

void TRIAC_RegisterFullCycleEndCallback( void( * func_ptr)( void ) )
{
	if(func_ptr)
	{
		TRIAC_fullCycleEndCallback = func_ptr;
	}
}

void TRIAC_setPower(uint8_t power)
{
	if (power > 100)
	{
		heaterPower = 100;
	}
	else
	{
		heaterPower = power;
	}
}

void TRIAC_zeroCrossEventHandler(void)
{

}

void TRIAC_powerControlRoutine(void)
{
	static uint8_t halfWaveCnt = 1;
	static uint8_t triacPower = 0;
	if( halfWaveCnt < triacPower )
	{
		TRIAC_on();
	}
	else
	{
		TRIAC_off();
	}

	halfWaveCnt++;
	if(halfWaveCnt > 100)
	{
		halfWaveCnt = 1;
		cycleUpdated = 1;
		triacPower = heaterPower;
	}
}

