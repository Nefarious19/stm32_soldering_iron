/*
 * led.c
 *
 *  Created on: 26.02.2019
 *      Author: rafal
 */

#include "../../PERIPHS/LED/led.h"

#include "stm32f0xx.h"

void LED_Init(void)
{
	//LED
	GPIOA->MODER |= GPIO_MODER_MODER15_0;
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR15;
	LED_Off();
}

void LED_On(void)
{
	GPIOA->ODR |= (1<<15);
}

void LED_Off(void)
{
	GPIOA->ODR &= ~(1<<15);
}

void LED_Tog(void)
{
	GPIOA->ODR ^= (1<<15);
}
