/*
 * led.h
 *
 *  Created on: 26.02.2019
 *      Author: rafal
 */

#ifndef LED_LED_H_
#define LED_LED_H_

void LED_Init(void);
void LED_On(void);
void LED_Off(void);
void LED_Tog(void);

#endif /* LED_LED_H_ */
